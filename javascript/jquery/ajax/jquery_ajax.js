var posiciones = {
    "00310": {"longitud": "2.284549", "id": "00310", "latitud": "41.629237", "visible": true, "nombre": "Orange Clover"},
    "04844": {"longitud": "2.563909", "id": "04844", "latitud": "41.585019", "visible": true, "nombre": "Ona Cartr�"},
    "00438": {"longitud": "2.100423", "id": "00438", "latitud": "41.360448", "visible": true, "nombre": "Pack Bazar"},
    "04861": {"longitud": "2.006114", "id": "04861", "latitud": "41.434876", "visible": true, "nombre": "Etiquetas Adh SSL"},
    "04545": {"longitud": "2.092957", "id": "04545", "latitud": "41.558773", "visible": true, "nombre": "Creaciones y Distribuciones T�xtiles"},
    "02476": {"longitud": "1.871783", "id": "02476", "latitud": "41.755498", "visible": true, "nombre": "Formularis Vi�as"},
    "04938": {"longitud": "1.645457", "id": "04938", "latitud": "41.308181", "visible": true, "nombre": "Impresos Pened�s"},
    "03181": {"longitud": "2.008349", "id": "03181", "latitud": "41.568470", "visible": true, "nombre": "Enne Industrias Gr�ficas"}
}

var myOptions = {
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
};

var map = new google.maps.Map(document.getElementById("mapa"), myOptions);
map.markers = {};

$(document).ready(function() { 

    cargar_combo();

    // bind 'myForm' and provide a simple callback function 
    $('#myForm').submit(function() {
        buscar_user($('#seleccion').val());
        return false;
    });

    var gc = new google.maps.Geocoder();
    gc.geocode({
        address: "Monistrol de Montserrat"
    }, function (resultado, status){
	if (status == google.maps.GeocoderStatus.OK){
            map.setCenter(resultado[0].geometry.location);

            var marker = new google.maps.Marker({
		map: map,
		position: resultado[0].geometry.location,
		clickable: true
            });

            google.maps.event.addListener(marker, 'click', function() {
		var infoWindow = new google.maps.InfoWindow({
		    content: "Bio-Pack",
		    maxWidth: 200
		});
		infoWindow.open(map, marker);
            });
            
            var infoWindow = new google.maps.InfoWindow({
		content: "Bio-Pack",
		maxWidth: 200
            });
            infoWindow.open(map, marker);
	}
	else{
            alert("fallo");
	}
    });
});

function buscar_user(id){
    return servidor_buscar_user(id);
}

function cliente_buscar_user(id){

    if (map.markers[id] != undefined) return;

    data = get_posicion(id);

    $('#e_nombre').text(data.id + "-" + data.nombre);
    $('#e_longitud').text(data.longitud);
    $('#e_latitud').text(data.latitud);
    $('#e_visible').text(data.visible);

    var posicion = new google.maps.LatLng(parseFloat(data.latitud), parseFloat(data.longitud));

    var marker = new google.maps.Marker({
        map: map,
        position: posicion,
        draggable: true
    });

    map.markers[id] = marker;

    google.maps.event.addListener(marker, 'dragend', function() {
        marker.setMap(null);
        delete map.markers[id];
    });

    google.maps.event.addListener(marker, 'click', function() {

        if (marker.infoWindow != undefined) return;

        var infoWindow = new google.maps.InfoWindow({
	    content: data.id + "-" + data.nombre,
	    maxWidth: 200
        });
        infoWindow.open(map, marker);
        marker.infoWindow = infoWindow;

        google.maps.event.addListener(infoWindow, 'closeclick', function() {
	    delete marker.infoWindow;
        });
    });

    var infoWindow = new google.maps.InfoWindow({
        content: data.id + "-" + data.nombre,
        maxWidth: 200
    });
    infoWindow.open(map, marker);

    marker.infoWindow = infoWindow;

    google.maps.event.addListener(infoWindow, 'closeclick', function() {
        delete marker.infoWindow;
    });
}

function servidor_buscar_user(id){
    $.ajax({
        url: "http://juanmi.radiacrom.com:8080/user/" + id
    }).done(function(data) {

        if (map.markers[id] != undefined) return;

        $('#e_nombre').text(data.id + "-" + data.nombre);
        $('#e_longitud').text(data.longitud);
        $('#e_latitud').text(data.latitud);
        $('#e_visible').text(data.visible);

        var posicion = new google.maps.LatLng(parseFloat(data.latitud), parseFloat(data.longitud));

        var marker = new google.maps.Marker({
	    map: map,
	    position: posicion,
	    draggable: true,
	    clickable: true
        });

        map.markers[id] = marker;

        google.maps.event.addListener(marker, 'dragend', function() {
	    marker.setMap(null);
	    delete map.markers[id];
        });

        google.maps.event.addListener(marker, 'click', function() {
	    if (marker.infoWindow != undefined) return;

	    var infoWindow = new google.maps.InfoWindow({
                content: data.id + "-" + data.nombre,
                maxWidth: 200
	    });
	    infoWindow.open(map, marker);
	    marker.infoWindow = infoWindow;

	    google.maps.event.addListener(infoWindow, 'closeclick', function() {
                delete marker.infoWindow;
	    });
        });

        var infoWindow = new google.maps.InfoWindow({
	    content: data.id + "-" + data.nombre,
	    maxWidth: 200
        });
        infoWindow.open(map, marker);
        marker.infoWindow = infoWindow;

        google.maps.event.addListener(infoWindow, 'closeclick', function() {
	    delete marker.infoWindow;
        });

    }).error(function (jqXHR, textStatus, errorThrown){
        $('#e_nombre').text("");
        $('#e_longitud').text("");
        $('#e_latitud').text("");
        $('#e_visible').text("");
    });
}

function get_posicion(id){
    return posiciones[id];
}

function cargar_combo(){
    for(var i in posiciones){
        $('#seleccion').append("<option value=\"" + posiciones[i].id + "\">" + posiciones[i].nombre + "</option>");
    }         
}
