#include <QFile>
#include <QDebug>
#include <QCoreApplication>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "config.h"
#include "configloader.h"

ConfigLoader::ConfigLoader(QObject *parent) : QObject(parent)
{
}

Config ConfigLoader::loadFromFilePath(QString path)
{
    Config c;

    QFile f(path);
    if (!f.exists()) {
        QJsonObject js = createJsonObject(c);
        if (!saveObject(&f, js)) {
            qDebug() << "Error opening file for writing";
            QCoreApplication::quit();
        }
    }
    else {
        c = loadObject(&f);
    }

    return c;
}

QJsonObject ConfigLoader::createJsonObject(const Config& c) {
    QJsonObject js;
    QJsonArray fields;

    foreach(QString field, c.getFields())
        fields.append(field);

    js.insert("name", c.getName());
    js.insert("fields", fields);
    return js;
}

bool ConfigLoader::saveObject(QFile* f, QJsonObject js) {
    if (!f->open(QIODevice::WriteOnly)) {
        return false;
    }
    QJsonDocument doc(js);
    QTextStream st(f);
    st << doc.toJson();
    f->close();

    return true;
}

Config ConfigLoader::loadObject(QFile* f) {
    Config c;

    if (!f->open(QIODevice::ReadOnly)) {
        qDebug() << "Error opening file for reading";
        QCoreApplication::quit();
    }

    QTextStream st(f);
    QString data = st.readAll();

    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject obj = doc.object();
    if (obj.contains("name") && obj["name"].isString()) {
        c.setName(obj["name"].toString());
    }
    else {
        qDebug() << "Error parsing config json key [name]";
        QCoreApplication::quit();
    }

    if (obj.contains("fields") && obj["fields"].isArray()) {
        QStringList fields = QStringList();
        QJsonArray array = obj["fields"].toArray();
        for(int i = 0; i < array.size(); i++) {
            if (array[i].isString()) {
                fields.append(array[i].toString());
            }
            else {
                qDebug() << "Error parsing config json key [fields]";
                QCoreApplication::quit();
            }
        }
        c.setFields(fields);
    }
    else {
        qDebug() << "Error parsing config json key [fields]";
        QCoreApplication::quit();
    }

    return c;
}
