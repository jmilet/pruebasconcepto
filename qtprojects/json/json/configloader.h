#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include <QObject>

class Config;
class QFile;

class ConfigLoader : public QObject
{
    Q_OBJECT

private:

    QJsonObject createJsonObject(const Config &c);
    Config loadObject(QFile* f);
    bool saveObject(QFile* f, QJsonObject js);

public:

    explicit ConfigLoader(QObject *parent = nullptr);
    Config loadFromFilePath(QString path);

signals:

public slots:
};

#endif // CONFIGLOADER_H
