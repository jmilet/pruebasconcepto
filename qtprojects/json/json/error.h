#ifndef ERROR_H
#define ERROR_H

#include <QString>

class Error
{
private:
    int mCode = 0;
    QString mMsg = "";

public:
    Error();
    Error(int code, QString msg);
    int getCode() { return mCode; }
    QString getMsg() { return mMsg; }
    bool isError() { return mCode != 0; }
};

#endif // ERROR_H
