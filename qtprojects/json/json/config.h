#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QStringList>

class QDebug;

class Config {

private:

    QString mName = "default name";
    QStringList mFields = {"default field 1", "default field 2"};

public:

    QString getName() const { return mName; }
    void setName(QString name) { mName = name; }
    QStringList getFields() const { return mFields; }
    void setFields(QStringList fields) { mFields = fields; }
    friend QDebug operator<<(QDebug qd, const Config& config);
};

#endif // CONFIG_H
