#ifndef OPTION_H
#define OPTION_H


#include "error.h"

template <class T>
class Result
{
private:
    T mValue;
    Error mError;

public:
    Result(T mValue);
    Result(Error mError);
    T getValue() { return mValue; }
    Error getError() { return mError; }
};

template <class T>
Result<T>::Result(T value) : mValue(value) {
}

template <class T>
Result<T>::Result(Error error) : mError(error) {
}

#endif // OPTION_H
