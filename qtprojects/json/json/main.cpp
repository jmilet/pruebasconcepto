#include <QCoreApplication>
#include <QDebug>
#include "config.h"
#include "configloader.h"

int main(int argc, char *argv[])
{
    ConfigLoader cl;
    Config c = cl.loadFromFilePath("config.json");

    qDebug() << c;

    return 0;
}
