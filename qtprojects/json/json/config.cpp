#include <QDebug>
#include "config.h"

QDebug operator<<(QDebug qd, const Config& config) {
    qd.nospace().noquote() << QString("%1|%2").arg(config.mName).arg(config.mFields.join(","));
    return qd;
}
