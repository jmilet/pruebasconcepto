#ifndef TESTRESULT_H
#define TESTRESULT_H

#include <QObject>
#include <QtTest>

class TestResult : public QObject
{
    Q_OBJECT


public:
    TestResult();

private Q_SLOTS:
    void checkValue();
    void checkError();
};

#endif // TESTRESULT_H
