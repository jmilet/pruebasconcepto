#-------------------------------------------------
#
# Project created by QtCreator 2018-03-07T14:24:21
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_jsontesttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        ../json/error.cpp \
        ../json/result.cpp \
        ../json/config.cpp \
        testerror.cpp \
        testresult.cpp \
        testconfig.cpp \
        main.cpp


DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    testerror.h \
    testresult.h \
    testconfig.h
