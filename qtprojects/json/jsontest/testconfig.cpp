#include <QTest>
#include <QString>
#include <QStringList>
#include <QDebug>
#include "testconfig.h"
#include "../json/config.h"

TestConfig::TestConfig()
{
}

void TestConfig::checkSettersGetters() {
    Config c;
    QString name = "test name";
    QStringList fields = {"f1", "f2", "f3"};

    c.setName(name);
    c.setFields(fields);

    QVERIFY2(name == c.getName(), "Unexpected name value");
    QVERIFY2(fields == c.getFields(), "Unexpected fields value");
}

void TestConfig::checkQDebug() {
    QString buffer;
    QDebug qd(&buffer);

    Config c;

    qd << c;

    QString expectedBuffer = QString("%1|%2").arg(c.getName()).arg(c.getFields().join(","));

    qDebug() << c;
    qDebug() << buffer;
    qDebug() << expectedBuffer;

    QVERIFY2(expectedBuffer == buffer, "Unexpected value of qDebug's buffer");
}
