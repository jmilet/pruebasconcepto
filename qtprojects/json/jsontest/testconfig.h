#ifndef TESTCONFIG_H
#define TESTCONFIG_H

#include <QObject>
#include <QtTest>

class TestConfig : public QObject
{
    Q_OBJECT

public:
    TestConfig();

private Q_SLOTS:

    void checkSettersGetters();
    void checkQDebug();
};

#endif // TESTCONFIG_H
