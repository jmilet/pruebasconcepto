#include <QTest>
#include "../json/result.h"
#include "testresult.h"

TestResult::TestResult()
{

}

void TestResult::checkValue() {
    Result<int> res(100);

    QVERIFY2(0 == res.getError().getCode(), "Expected error code zero");
    QVERIFY2("" == res.getError().getMsg(), "Expected empty error message");
    QVERIFY2(100 == res.getValue(), "Got unexpected value");
}

void TestResult::checkError() {
    int errCode = 100;
    QString errMsg = "testing error";
    Error err(errCode, errMsg);
    Result<int> res(err);

    QVERIFY2(true == res.getError().isError(), "Unexpected isError value");
    QVERIFY2(errCode == res.getError().getCode(), "Unexpected error code");
    QVERIFY2(errMsg == res.getError().getMsg(), "Unexpected message");
}
