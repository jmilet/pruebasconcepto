#include <QtTest>
#include "testerror.h"
#include "testresult.h"
#include "testconfig.h"

int main(int argc, char** argv)
{
   int status = 0;

   {
      TestError tc;
      status |= QTest::qExec(&tc, argc, argv);
   }

   {
      TestResult tc;
      status |= QTest::qExec(&tc, argc, argv);
   }

   {
      TestConfig tc;
      status |= QTest::qExec(&tc, argc, argv);
   }

   return status;
}
