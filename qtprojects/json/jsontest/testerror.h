#ifndef TESTERROR_H
#define TESTERROR_H

#include <QObject>
#include <QtTest>

class TestError : public QObject
{
    Q_OBJECT

public:
    TestError();

private Q_SLOTS:
    void checkDefaultErrorValues();
    void checkSetValues();
    void checkIsErrorWhenOK();
    void checkIsErrorWhenError();
};

#endif // TESTERROR_H
