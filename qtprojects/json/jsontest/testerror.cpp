#include <QtTest>
#include "../json/error.h"
#include "testerror.h"


TestError::TestError()
{
}

void TestError::checkDefaultErrorValues()
{
    Error err;
    QVERIFY2(0 == err.getCode(), "Unexpected error code");
    QVERIFY2("" == err.getMsg(), "Unexpected msg");
}

void TestError::checkSetValues()
{
    Error err(100, "OK");
    QVERIFY2(100 == err.getCode(), "Unexpected error code");
    QVERIFY2("OK" == err.getMsg(), "Unexpected msg");
}

void TestError::checkIsErrorWhenOK()
{
    Error err;
    QVERIFY2(false == err.isError(), "Expected not error and got error");
}

void TestError::checkIsErrorWhenError()
{
    Error err(100, "provoked error");
    QVERIFY2(true == err.isError(), "Expected not error and got error");
}

//QTEST_APPLESS_MAIN(TestError)


