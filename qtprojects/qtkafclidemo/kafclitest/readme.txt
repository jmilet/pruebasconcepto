https://stackoverflow.com/questions/12684152/how-can-i-set-up-my-project-hierarchy-in-qtcreator
https://stackoverflow.com/questions/3492739/auto-expanding-layout-with-qt-designer

http://doc.qt.io/qtcreator/creator-project-creating.html#adding-subprojects-to-projects
http://doc.qt.io/qt-5/qtest.html#QCOMPAREx
http://doc.qt.io/qtcreator/creator-editor-locator.html
http://doc.qt.io/qtcreator/creator-keyboard-shortcuts.html
https://wiki.qt.io/Qt_Creator_Keyboard_Shortcuts


chage .h <=> .cpp   F4
bookmark:           ctrl+m, ctrl+.
build:              alt+b
run:                alt+r
locator:            alt+k  ==>  p pattern (looks for files in the project)
                                o pattern (looks for open files)
next file:          alt+tab
run all tests:      alt+shift+t, alt+a
run current tests:  alt+shift+t, alt+r
select all:         ctrl+a
code formating:     ctrl+i   =>   ctrl+a, ctrl+i
rename var:         ctrl+shift+r

-----------------------------------------------------------------------------------------------------

qWarning("text");                           // It doesn't accept QString objects.
qWarning() << qstringObj;                   //
qWarning() << "a" << "b";                   // Insers spaces between elements.
qWarning().nospace() << "a" << "b";         // No spaces.
msg.toStdString().c_str();                  // From Qstring to cstring, same as qPrintable(msg);

-----------------------------------------------------------------------------------------------------

connect(w, &QPushButton::clicked, this, &QPushButton::close);

QMAKE_CXXFLAGS += -std=c++11

connect(w, &QPushButton::clicked, this, [this](){
    this.close();
});

w->disconnect();

w->setText(tr("text")); <- to get tranlation.

-----------------------------------------------------------------------------------------------------

Balsamiq -> mockup tool.

-----------------------------------------------------------------------------------------------------

Model
View
    mainview.cpp
    mainview.h
    mainview.ui
ViewMgr

-----------------------------------------------------------------------------------------------------

#include <utility>
typedef std::pair<A, B> ObjErr;
return std::make_pair(obj, err);

#include <memory>
std::unique_ptr<Object> obj;

-----------------------------------------------------------------------------------------------------

QStandardPaths
QString::number();
text().toInt();
signal: editingFinished();

