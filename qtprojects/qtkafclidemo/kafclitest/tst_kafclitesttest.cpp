#include <QString>
#include <QtTest>
#include "../kafcli/own_qnetwork_access_manager.h"

class KafclitestTest : public QObject
{
    Q_OBJECT

private:
    OwnQNetworkAccessManager *netManager;

public:
    KafclitestTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();
    void testCase1();
    void testCase2();
};

KafclitestTest::KafclitestTest()
{
}

void KafclitestTest::initTestCase()
{
    netManager = new OwnQNetworkAccessManager();
    qDebug() << "Run just once - init";
}

void KafclitestTest::cleanupTestCase()
{
    delete netManager;
    qDebug() << "Run just once - cleanup";
}

void KafclitestTest::init()
{
    qDebug() << "Run just per test - init";
}

void KafclitestTest::cleanup()
{
    qDebug() << "Run just per test - cleanup";
}

void KafclitestTest::testCase1()
{
    QVERIFY(netManager->call());
}

void KafclitestTest::testCase2()
{
    QCOMPARE(netManager->getSomeString(), QString("hello"));
}

QTEST_APPLESS_MAIN(KafclitestTest)

#include "tst_kafclitesttest.moc"
