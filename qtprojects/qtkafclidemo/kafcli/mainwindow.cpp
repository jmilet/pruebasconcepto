#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent, QNetworkAccessManager *netManager) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->netManager = netManager;
    connect(ui->sendButton, SIGNAL(clicked(bool)), this, SLOT(sendText(bool)));

    QStringList elements = {"hola", "que", "tal"};
    model.setStringList(elements);
    ui->listText->setModel(&model);

    // Added the lambda to play.
    // Just one of the callbacks can read the request's content.
    connect(netManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(requestFinished(QNetworkReply*)));
    connect(netManager, &QNetworkAccessManager::finished, this, [](QNetworkReply* reply){
        qDebug() << reply->error();
        qDebug() << reply->readAll();
    });
}

void MainWindow::requestFinished(QNetworkReply *reply)
{
    qDebug() << reply->error();
    qDebug() << reply->readAll();
}

void MainWindow::sendText(bool checked)
{
    QString s = ui->textTextEdit->toPlainText();
    if (!s.isEmpty()) {

        /*
         * There are two different ways of doing this.
         *
         *   1. the following commented one indexing the model.
         *   2. replacing the string model list.
         *
            model.insertRow(model.rowCount());
            QModelIndex index = model.index(model.rowCount() - 1, 0);
            model.setData(index, s);
        */

        model.setStringList(model.stringList() << s);

        qDebug() << "clicked: " << s << "|" << model.stringList();
    }

    netManager->get(QNetworkRequest(QUrl("https://google.com")));
}

MainWindow::~MainWindow()
{
    delete ui;
}
