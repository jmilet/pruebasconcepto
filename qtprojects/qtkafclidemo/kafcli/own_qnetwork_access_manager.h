#ifndef REQUESTER_H
#define REQUESTER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>

class OwnQNetworkAccessManager : public QNetworkAccessManager
{
public:
    OwnQNetworkAccessManager();
    bool call();
    QString getSomeString();
};

#endif // REQUESTER_H
