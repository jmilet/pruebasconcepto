#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QNetworkAccessManager netManager;
    MainWindow w(NULL, &netManager);
    w.show();

    return a.exec();
}
