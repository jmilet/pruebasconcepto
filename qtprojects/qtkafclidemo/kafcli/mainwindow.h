#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include "own_qnetwork_access_manager.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent, QNetworkAccessManager *netManager);
    ~MainWindow();

private slots:
    void sendText(bool checked);
    void requestFinished(QNetworkReply *reply);

private:
    Ui::MainWindow *ui;
    QStringListModel model;
    QNetworkAccessManager *netManager;
};

#endif // MAINWINDOW_H
