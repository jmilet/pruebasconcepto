#ifndef ISERVICEMANAGER_H
#define ISERVICEMANAGER_H

#include <QObject>


class QByteArray;


class ServiceManager : public QObject
{
    Q_OBJECT

public:
    enum ResponseCode {
        OK,
        ERROR
    };

    ServiceManager(QObject* parent) : QObject(parent){}
    virtual void call(QString rul) = 0;

signals:
    void serviceFinished();
    void serviceRead(ResponseCode ret, QByteArray data);
};

#endif // ISERVICEMANAGER_H
