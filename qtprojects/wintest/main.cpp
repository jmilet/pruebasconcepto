#include <QApplication>
#include "mainwindow.h"
#include "servicemanagerimpl.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ServiceManager* service = new ServiceManagerImpl(&a);
    MainWindow w(nullptr, service);
    w.show();

    return a.exec();
}
