#include <QDebug>
#include <QPushButton>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QMessageBox>
#include <QModelIndex>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent, ServiceManager *service) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mService(service),
    mNumPacketsReceived(0),
    mNumBytesReceived(0)
{
    ui->setupUi(this);

    defaultState();
    ui->listMessages->setModel(&mListModel);

    connect(ui->btnCancel, &QPushButton::clicked, QApplication::instance(), &QApplication::quit);
    connect(ui->btnSend, &QPushButton::clicked, this, &MainWindow::sendCommand);
    connect(mService, &ServiceManager::serviceFinished, this, &MainWindow::serviceFinished);
    connect(mService, &ServiceManager::serviceRead, this, &MainWindow::serviceRead);
}

MainWindow::~MainWindow()
{
    qDebug() << "delete MainWindow";
    delete ui;
}

void MainWindow::widgetsEnabled(bool enabled)
{
    ui->grbServer->setEnabled(enabled);
    ui->grbResponse->setEnabled(enabled);
    ui->btnSend->setEnabled(enabled);
}

void MainWindow::defaultState()
{
    ui->entryUrl->setText("https://www.google.com");
}

void MainWindow::sendCommand(bool checked) {
    resetCounters();

    QString text = ui->entryUrl->text().trimmed();
    qDebug() << text.isEmpty();
    qDebug() << QUrl(text).isValid();
    qDebug() << "clicked";

    if (text.isEmpty() || !QUrl(text).isValid()) {
        QMessageBox msg;
        msg.setText(tr("Invalid URL"));
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }
    else {
        widgetsEnabled(false);
        mService->call(QString(ui->entryUrl->text()));
    }
}

void MainWindow::serviceFinished() {
    widgetsEnabled(true);
    updateProgress();
    //qDebug() << "response:" << data;
}

void MainWindow::serviceRead(ServiceManager::ResponseCode ret, QByteArray data) {
    mNumPacketsReceived++;
    mNumBytesReceived += data.length();

    qDebug() << data.length();

    if (mNumPacketsReceived % 1000 != 0) return;

    updateProgress();
}

void MainWindow::updateProgress() {
    int row = mListModel.rowCount();
    mListModel.insertRow(row);
    QModelIndex index = mListModel.index(row, 0);
    mListModel.setData(index, tr(qPrintable(QString("received %1 packets, %2 bytes received").arg(mNumPacketsReceived).arg(mNumBytesReceived))));
}

void MainWindow::resetCounters()
{
    mNumPacketsReceived = 0;
    mNumBytesReceived = 0;
}
