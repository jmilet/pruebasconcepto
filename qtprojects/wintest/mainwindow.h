#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include "servicemanager.h"

namespace Ui {
class MainWindow;
}

class QStringListModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent, ServiceManager* service);
    ~MainWindow();

private:
    Ui::MainWindow* ui;
    ServiceManager* mService;
    QStringListModel mListModel;
    long mNumPacketsReceived;
    long mNumBytesReceived;

    void widgetsEnabled(bool enabled);
    void defaultState();
    void updateProgress();
    void resetCounters();

public slots:
    void sendCommand(bool checked);
    void serviceFinished();
    void serviceRead(ServiceManager::ResponseCode ret, QByteArray data);
};

#endif // MAINWINDOW_H
