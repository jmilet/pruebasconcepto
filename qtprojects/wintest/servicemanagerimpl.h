#ifndef SERVICEMANAGERIMPL_H
#define SERVICEMANAGERIMPL_H

#include "servicemanager.h"

class QNetworkReply;
class QNetworkAccessManager;

class ServiceManagerImpl : public ServiceManager
{

private:
    QNetworkAccessManager* mNam;
    QNetworkReply* mReply;

public:
    ServiceManagerImpl(QObject* parent);
    ~ServiceManagerImpl();
    void call(QString url) override;

public slots:
    void httpFinished(QNetworkReply* reply);
    void readyRead();
};

#endif // SERVICEMANAGERIMPL_H
