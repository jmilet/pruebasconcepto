#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include "servicemanagerimpl.h"

ServiceManagerImpl::ServiceManagerImpl(QObject *parent) :
    ServiceManager(parent),
    mNam(new QNetworkAccessManager(this))
{
    mNam->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    connect(mNam, &QNetworkAccessManager::finished, this, &ServiceManagerImpl::httpFinished);
}

ServiceManagerImpl::~ServiceManagerImpl() {
    qDebug() << "delete ServiceManagerImpl";
}

void ServiceManagerImpl::call(QString url) {
    mReply = mNam->get(QNetworkRequest(QUrl(url)));
    connect(mReply, &QIODevice::readyRead, this, &ServiceManagerImpl::readyRead);
    qDebug() << "reply in call:" << mReply;
}

void ServiceManagerImpl::httpFinished(QNetworkReply* reply)
{
    qDebug() << "reply in httpFinished:" << reply;
    mReply->deleteLater();
    emit serviceFinished();
}

void ServiceManagerImpl::readyRead() {
    qDebug() << "readyRead()";

    if (mReply->error() == QNetworkReply::NoError) {
        QByteArray content= mReply->readAll();
        emit serviceRead(ServiceManager::OK, content);
    }
    else {
        qDebug() << "Error in http call" << mReply->errorString();
        emit serviceRead(ServiceManager::ERROR, mReply->errorString().toUtf8());
    }
}
