#include <QObject>

#ifndef TST_GENERAL_H
#define TST_GENERAL_H

class General : public QObject
{
    Q_OBJECT

public:
    static const constexpr char* dateTimeFormat = "yyyyMMdd hhmmss";

    General();
    ~General();

private slots:
    void test_case1();
};

#endif // TST_GENERAL_H
