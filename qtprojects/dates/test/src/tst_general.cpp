#include <QtTest>
#include "../inc/tst_general.h"

General::General()
{

}

General::~General()
{

}

void General::test_case1()
{
    auto d1 = QDateTime::fromString("20180312 080000", General::dateTimeFormat);
    auto d2 = d1;
    auto d3 = QDateTime::fromString("20180312 080000", General::dateTimeFormat);

    d2 = d2.addSecs(1);

    QVERIFY2(d1 < d2, "Expected d1 to be lower than d2");
    QVERIFY2(d2 > d1, "Expected d2 to be lower than d1");
    QVERIFY2(d1 == d3, "Expected d2 to be equal to d1");

    qDebug() << d1.toString(General::dateTimeFormat);
}

//QTEST_APPLESS_MAIN(General)

//#include "tst_general.moc"
