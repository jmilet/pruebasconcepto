#include <QTest>
# include "../inc/tst_general.h"

// http://doc.qt.io/qt-5/qtest.html

//#include "tst_general.moc"


int main(int argc, char* argv[]) {
    int ret = 0;

    General t1;
    ret |= QTest::qExec(&t1, argc, argv);

    return ret;
}
