#include <QCoreApplication>
#include <QDebug>
#include <QString>
#include <memory>

class BigClass {
public:
    BigClass() : field("") {}
    char field[1024 * 512];
};

typedef std::unique_ptr<BigClass> P;
typedef std::shared_ptr<BigClass> SP;

void uniquePassFoo(P& f) {
    qDebug() << qPrintable(QString("passFoo: '%1' - '%2'").arg(f.get() != nullptr).arg(f->field));
}

P uniqueMoveAndReturnFoo(P f) {
    qDebug() << qPrintable(QString("moveAndReturnFoo: '%1' - '%2'").arg(f.get() != nullptr).arg(f->field));
    return f;
}

void uniqueMoveFoo(P f) {
    qDebug() << qPrintable(QString("moveFoo: '%1' - '%2'").arg(f.get() != nullptr).arg(f->field));
}

void runUniqueTest() {
    while(true) {
        auto f = P(new BigClass);
        strcpy(f->field, "a given value");
        uniquePassFoo(f);
        f = uniqueMoveAndReturnFoo(std::move(f));
        uniqueMoveFoo(std::move(f));
        qDebug() << qPrintable(QString("main: '%1'").arg(f.get() != nullptr));
        qDebug("----------------------------------------");
//        break;
    }
}

void sharedPassFoo(SP f) {
    qDebug() << qPrintable(QString("sharedPassFoo: '%1' - '%2' -> use count '%3'").arg(f.get() != nullptr).arg(f->field).arg(f.use_count()));
}

void runSharedTest() {
    while(true) {
        QMap<QString, SP> map1;
        QMap<QString, SP> map2;

        auto f = SP(new BigClass);
        strcpy(f->field, "OK");

        map1["one"] = f;
        map2["two"] = f;

        sharedPassFoo(f);
        qDebug() << QString("Use count %1").arg(f.use_count());
//        break;
    }
}

void checkConstReference(const QString& q) {
    q.at(0);
}


int main(int argc, char *argv[])
{
//    QCoreApplication a(argc, argv);
//    return a.exec();

//    runUniqueTest();
    runSharedTest();

//    QString q("A");
//    checkConstReference(q);
//    qDebug() << q;

}
