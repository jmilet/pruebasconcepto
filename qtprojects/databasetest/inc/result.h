#ifndef RESULT_H
#define RESULT_H

#include <QString>

template <class T>
class Result {

private:
    int mErrorCode;
    QString mMessage;
    T mValue;

public:
    Result(int errorCode, QString msg) : mErrorCode(errorCode), mMessage(msg) {}
    Result(T value) : mErrorCode(0), mValue(value) {}

    bool isError() { return mErrorCode != 0; }
    QString getErrorMessage() { return mMessage; }
    T getValue() { return mValue; }
};

#endif // RESULT_H
