#ifndef DAO_H
#define DAO_H

#include "result.h"

class Dao
{
public:
    virtual bool createTable() = 0;
    virtual bool insertRow() = 0;
    virtual Result<long> selectCount() = 0;
    virtual Result<QStringList> selectByRange(long a, long b) = 0;
};

#endif // DAO_H
