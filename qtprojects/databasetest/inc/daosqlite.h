#ifndef DAOSQLITE_H
#define DAOSQLITE_H

#include <QSqlDatabase>
#include "database.h"
#include "dao.h"


class DaoSqlite : public Dao
{

private:
    Database<QSqlDatabase>& mDb;

public:
    DaoSqlite(Database<QSqlDatabase>& db);
    bool createTable() override;
    bool insertRow() override;
    Result<long> selectCount() override;
    Result<QStringList> selectByRange(long a, long b) override;
};

#endif // DAOSQLITE_H
