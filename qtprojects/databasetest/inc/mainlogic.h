#ifndef MAINLOGIC_H
#define MAINLOGIC_H

#include "result.h"
#include "dao.h"

class MainLogic
{
public:
    MainLogic();
    Result<int> run(Dao& dao);
};

#endif // MAINLOGIC_H
