#ifndef DATABASE_H
#define DATABASE_H


template <class T>
class Database {

private:
    T& mDb;

public:
    Database(T& db) : mDb(db) {}
    T& getDb() { return mDb; }
    void setDb(T& db) { mDb = db; }
};

#endif // DATABASE_H
