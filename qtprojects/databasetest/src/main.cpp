#include "../inc/mainwindow.h"
#include <QApplication>
#include <QSqlDatabase>
#include <QTemporaryDir>
#include <QDebug>

#include "../inc/database.h"
#include "../inc/daosqlite.h"
#include "../inc/result.h"
#include "../inc/mainlogic.h"

// https://isocpp.org/wiki/faq
// https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file
// https://stackoverflow.com/questions/7817864/open-existing-sqlite3-database-under-qt


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // File name.
    const QString dataFile(":memory:");
    //    QTemporaryDir dir;
    //    dir.setAutoRemove(false);
    //    const QString dataFile = dir.path() + "/" + "qdatafile.dat";
    qDebug() << dataFile;

    // Open database.
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dataFile);
    if (!db.open()) {
        qDebug() << "Could not open the database";
        return -1;
    }

    Database<QSqlDatabase> wDb(db);
    DaoSqlite dao(wDb);

    MainLogic().run(dao);

//    MainWindow w;
//    w.show();
//    return a.exec();

    return 0;
}
