#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QStringList>
#include "../inc/daosqlite.h"

DaoSqlite::DaoSqlite(Database<QSqlDatabase>& db) : mDb(db)
{
}

bool DaoSqlite::createTable() {
    auto db = mDb.getDb();

    QSqlQuery query(db);
    query.prepare("CREATE TABLE IF NOT EXISTS TEST (id INTEGER PRIMARY KEY AUTOINCREMENT, DESC TEXT)");
    if (!query.exec()) {
       qDebug("Error creating table TEST");
       qDebug() << db.lastError().text();
       return false;
   }

   return true;
}

bool DaoSqlite::insertRow() {
    auto db = mDb.getDb();

    QSqlQuery query(db);
    query.prepare("INSERT INTO TEST (DESC) VALUES ('It works')");
    if (!query.exec()) {
       qDebug("Error inserting into table TEST");
       qDebug() << db.lastError().text();
       return false;
   }

   return true;
}

Result<long> DaoSqlite::selectCount() {
    auto db = mDb.getDb();

    QSqlQuery query(db);
    query.prepare("SELECT COUNT(*) FROM TEST");
    if (!query.exec()) {
       qDebug("Error select count TEST");
       auto error = db.lastError().text();
       return Result<long>(-1, error);
   }

   if (!query.first()) Result<long>(-1);

   return Result<long>(query.value(0).toInt());
}

Result<QStringList> DaoSqlite::selectByRange(long a, long b) {
    auto db = mDb.getDb();
    QStringList ret;

    QSqlQuery query(db);
    query.prepare("SELECT ID, DESC FROM TEST WHERE ID BETWEEN :a AND :b");
    query.bindValue(":a", QVariant((unsigned long long) a));
    query.bindValue(":b", QVariant((unsigned long long) b));
    if (!query.exec()) {
       qDebug("Error select by range TEST");
       auto error = db.lastError().text();
       qDebug() << error;
       return Result<QStringList>(-1, error);
    }

    while (query.next()) {
        ret.append(QString("%1 => %2").arg(query.value(0).toString(), query.value(1).toString()));
    }

    return Result<QStringList>(ret);
}
