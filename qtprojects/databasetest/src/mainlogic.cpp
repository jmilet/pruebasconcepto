#include <QDebug>
#include "../inc/mainlogic.h"
#include "../inc/result.h"
#include "../inc/dao.h"

MainLogic::MainLogic()
{

}

Result<int> MainLogic::run(Dao& dao) {
    auto ret = Result<int>(-1);

    // Create table and some operations.
    if (dao.createTable()) {
        qDebug() << "Table created";
    }
    else {
        qDebug() << "Could not create table";
        return ret;
    }

    for(int i = 0; i < 1000000; i++) {
        if (!dao.insertRow()) {
            qDebug() << "Could not insert row";
            return ret;
        }
    }

    // Count.
    {
        auto res = dao.selectCount();
        if (!res.isError()) {
            qDebug() << "Count: " << res.getValue();
        }
        else {
            qDebug() << "Could not select count" << res.getErrorMessage();
            return ret;
        }
    }

    // Select by range.
    {
        auto res = dao.selectByRange(20, 30);
        if (!res.isError()) {
            foreach(auto d, res.getValue()) {
                qDebug() << d;
            }
        }
        else {
            qDebug() << "Could not select range" << res.getErrorMessage();
            return ret;
        }
    }

    ret = Result<int>(0);

    return ret;
}
