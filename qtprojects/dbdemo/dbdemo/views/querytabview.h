#ifndef QUERYTABVIEW_H
#define QUERYTABVIEW_H

#include <QWidget>

namespace Ui {
class QueryTabView;
}

class QueryTabView : public QWidget
{
    Q_OBJECT

public:
    explicit QueryTabView(QWidget *parent = 0);
    ~QueryTabView();

private:
    Ui::QueryTabView *ui;
};

#endif // QUERYTABVIEW_H
