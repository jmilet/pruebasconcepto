#include "mainview.h"
#include "querytabview.h"
#include "ui_mainview.h"
#include <QDebug>

MainView::MainView(QWidget *parent, QueryTabView& queryTabView) :
    QMainWindow(parent),
    m_queryTabView(queryTabView),
    ui(new Ui::MainView)
{
    ui->setupUi(this);
    queryTabView.setParent(this);
    ui->loTab->addWidget(&queryTabView);
//    auto m = new QueryTabView(this);
//    ui->loTab->addWidget(m);
    //m_queryTabView.setParent(this);
    //ui->loTabDb->addWidget(&m_queryTabView);
}

MainView::~MainView()
{
    qDebug() << "delete MainView";
    delete ui;
}
