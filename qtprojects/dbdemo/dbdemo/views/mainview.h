#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QMainWindow>

namespace Ui {
class MainView;
}

class QueryTabView;

class MainView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainView(QWidget *parent, QueryTabView& queryTabView);
    ~MainView();

private:
    QueryTabView& m_queryTabView;
    Ui::MainView *ui;
};

#endif // MAINVIEW_H
