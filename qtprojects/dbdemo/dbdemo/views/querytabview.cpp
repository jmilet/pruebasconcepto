#include "querytabview.h"
#include "ui_querytabview.h"
#include <QDebug>

QueryTabView::QueryTabView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QueryTabView)
{
    ui->setupUi(this);
}

QueryTabView::~QueryTabView()
{
    qDebug() << "delete QueryTabView";
    delete ui;
}
