#include "startup.h"
#include "views/mainview.h"
#include "views/querytabview.h"
#include <QDebug>

Startup::Startup() :
    QObject(nullptr),
    m_queryTabView(*new QueryTabView(nullptr)),
    m_mainView(*new MainView(nullptr, m_queryTabView))
{
}

Startup::~Startup()
{
    delete &m_mainView;
    qDebug() << "delete Startup";
}

void Startup::show() const
{
    m_mainView.show();
}
