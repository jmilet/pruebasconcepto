#ifndef STARTUP_H
#define STARTUP_H

#include <QObject>

class MainView;
class QueryTabView;
class DbManager;

class Startup : public QObject
{
    Q_OBJECT

private:
    QueryTabView& m_queryTabView;
    MainView& m_mainView;
    DbManager& m_dbManager;

public:
    explicit Startup();
    ~Startup();
    void show() const;

signals:

public slots:
};

#endif // STARTUP_H
