#include "../inc/luainterface.h"
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QDir>
#include <QDebug>

LuaInterface::LuaInterface() {
    QString luaCode = "function add ()\n   return 100\nend";
    createLuaCode(luaCode);

    // Initialize Lua.
    L = luaL_newstate();

    // Load Lua base libraries.
    luaL_openlibs(L);
}

LuaInterface::~LuaInterface() {
    // Cleanup Lua.
    lua_close(L);
}

int LuaInterface::call() {
    int sum;

    // load the script.
    const char* cFilePath = getFilePath().toUtf8().data();
    luaL_dofile(L, cFilePath);

    // The function name.
    lua_getglobal(L, "add");

    // The first argument.
    //lua_pushnumber(L, x);

    // The second argument.
    //lua_pushnumber(L, y);

    // Call the function with 2 arguments, return 1 result.
    //lua_call(L, 2, 1);
    lua_call(L, 0, 1);

    // Get the result.
    sum = (int)lua_tointeger(L, -1);
    lua_pop(L, 1);

    return sum;
}

void LuaInterface::createLuaCode(const QString code) {
    QFile file(getFilePath());
    file.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream st(&file);
    st << code;
}

QString LuaInterface::readLuaCode() {
    QFile file(getFilePath());
    file.open(QFile::ReadOnly);
    QTextStream st(&file);
    return st.readAll();
}

QString LuaInterface::getFilePath() {
    return QDir::currentPath() + "/code.lua";
}
