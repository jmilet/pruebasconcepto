#include "../inc/dialogone.h"
#include "../inc/luainterface.h"
#include "ui_dialogone.h"
#include <QDebug>


DialogOne::DialogOne(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOne)
{
    ui->setupUi(this);

    QString code = mLua.readLuaCode();
    ui->editText->setText(code);

    connect(ui->pbCallLua, &QPushButton::clicked, this, &DialogOne::callLua);
    connect(ui->pbButtons, &QDialogButtonBox::accepted, this, &DialogOne::accepted);
    connect(ui->pbButtons, &QDialogButtonBox::rejected, this, &DialogOne::rejected);
}

DialogOne::~DialogOne()
{
    delete ui;
}

void DialogOne::callLua() {
    mLua.createLuaCode(mText = ui->editText->toPlainText());
    qDebug() << mLua.call();
}

void DialogOne::accepted() {
    mText = ui->editText->toPlainText();
}

void DialogOne::rejected() {
    ui->editText->setText("");
}

QString DialogOne::getText() {
    return mText;
}

void DialogOne::SetText(QString text)
{
    ui->editText->setText(text);
}

