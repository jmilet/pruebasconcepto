#include "../inc/mainwindow.h"
#include "../inc/dialogone.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pbOpen, &QPushButton::clicked, this, &MainWindow::openClicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openClicked(bool checked) {
    qDebug() << "clicked";

    DialogOne dlg(this);
    if (dlg.exec()) {
        qDebug() << dlg.getText();
    }
}
