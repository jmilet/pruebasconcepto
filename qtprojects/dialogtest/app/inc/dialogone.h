#ifndef DIALOGONE_H
#define DIALOGONE_H

#include <QDialog>
#include "../inc/luainterface.h"

namespace Ui {
class DialogOne;
}

class DialogOne : public QDialog
{
    Q_OBJECT

private:
    QString mText;
    LuaInterface mLua;

public:
    explicit DialogOne(QWidget *parent = 0);
    QString getText();
    void SetText(QString text);
    ~DialogOne();

public slots:
    void accepted();
    void rejected();
    void callLua();

private:
    Ui::DialogOne *ui;
};

#endif // DIALOGONE_H
