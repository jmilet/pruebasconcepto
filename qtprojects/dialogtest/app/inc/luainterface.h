#ifndef LUAINTERFACE_H
#define LUAINTERFACE_H

extern "C" {
        #include "../inc/lua/lua.h"
        #include "../inc/lua/lualib.h"
        #include "../inc/lua/lauxlib.h"
}

class QString;

class LuaInterface
{
private:
    // The Lua interpreter.
    lua_State* L;

    QString getFilePath();

public:
    LuaInterface();
    ~LuaInterface();
    int call();
    void createLuaCode(const QString code);
    QString readLuaCode();
};

#endif // LUAINTERFACE_H
