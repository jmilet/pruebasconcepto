package main

import (
	"log"
	"sync"
	"time"
)

type myFunc func()

type workPool struct {
	wg        sync.WaitGroup
	workQueue chan myFunc
}

func (wp *workPool) loop() {
	wp.wg.Add(1)
	defer wp.wg.Done()

	for w := range wp.workQueue {
		w()
	}
}

func (wp *workPool) wait() {
	wp.wg.Wait()
}

func (wp *workPool) close() {
	close(wp.workQueue)
}

func newWorkPool() *workPool {
	wp := workPool{}
	wp.workQueue = make(chan myFunc)
	return &wp
}

func main() {

	wp := newWorkPool()

	for i := 0; i < 5; i++ {
		go wp.loop()
	}

	for i := 0; i < 50; i++ {
		j := i
		wp.workQueue <- func() {
			log.Println(j)
			time.Sleep(200 * time.Millisecond)
		}
	}
	wp.close()
	wp.wait()

	log.Println("Done")
}
