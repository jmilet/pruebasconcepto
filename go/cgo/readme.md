# Go / C interaction
The project consists of:

## Description

* thecthing: The C thing is a library writen in C.
* thegothing: The Go thing is a library writen in Go.
* thecprogram: The C program is a progran which uses the Go library.
* thegoprogram: The Go program is a program which uses the C library.

## Compile

```bash
make
makeclean
```

## Usage

```bash
./thecprogram
./thegoprogram
```

Be aware that both are infinite loop programs, just to check that no memory is leaked.
