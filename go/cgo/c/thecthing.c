#include <stdio.h>
#include <stdlib.h>
#include "thecthing.h"


TheData* new() {
    return malloc(sizeof(TheData));
}

void print_stdout() {
    printf("it works from C!!!\n");
}

void show(TheData* d) {
    printf("x = %d\n", d->x);
    printf("y = %s\n", d->y);
}

void freeData(TheData* d) {
    free(d);
}