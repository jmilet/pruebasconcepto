#ifndef __THETHING
#define __THETHING

typedef struct {
    int x;
    char* y;
} TheData;

TheData* new();
void show(TheData* d);
void freeData(TheData* d);
void print_stdout();

#endif