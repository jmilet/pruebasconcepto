package main

// #include <stdlib.h>
// #include "thecthing.h"
// #cgo CFLAGS: -I ../../../c
// #cgo LDFLAGS: -L../../.. -lthecthing
// extern void showInGo(TheData* d);
import (
	"C"
)
import (
	"fmt"
	"unsafe"
)

func foo() {
	//C.print_stdout()

	d := C.new()
	defer C.freeData(d)

	d.x = 100
	d.y = C.CString("it works calling from Go")
	defer C.free(unsafe.Pointer(d.y))

	C.show(d)
}

//export showInGo
func showInGo(d *C.TheData) {
	fmt.Println("From Go: %d, %s", d.x, d.y)
}

func main() {
	for {
		foo()
	}
}
