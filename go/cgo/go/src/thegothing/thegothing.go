package main

// #include <stdlib.h>
// #include "thecthing.h"
// #cgo CFLAGS: -I ../../../c
import (
	"C"
)
import "fmt"

//export showInGo
func showInGo(d *C.TheData) {
	fmt.Printf("From Go: %d, %s\n", d.x, C.GoString(d.y))
}

func main() {}
