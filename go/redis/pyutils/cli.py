import http.client
import uuid

def create_value():
    return str(uuid.uuid1())

def call():
    key = create_value()
    value = create_value()
    conn = http.client.HTTPConnection("localhost:8080")
    conn.request("GET", "/set/%s/%s" % (key, value))
    r1 = conn.getresponse()
    # print(r1.status, r1.reason)
    conn.close()
    print((key, value))

def main():
    while True:
        call()

main()
