package cache

import (
	"time"
)

// MockCache is a mocking cache.
type MockCache struct {
	data map[string]interface{}
}

// NewMockCache creates a new recorder.
func NewMockCache() *MockCache {
	r := &MockCache{}
	r.data = make(map[string]interface{})
	return r
}

// Set sets a key value.
func (c *MockCache) Set(key string, value interface{}, expiration time.Duration) error {
	c.data[key] = value
	return nil
}

// Get returns the value of the given key.
func (c *MockCache) Get(key string) (interface{}, bool, error) {
	value, ok := c.data[key]
	return value, ok, nil
}

// Close closes the cache.
func (c *MockCache) Close() {
}
