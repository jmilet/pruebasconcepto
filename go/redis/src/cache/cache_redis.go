package cache

import (
	"time"

	"github.com/go-redis/redis"
)

// RedisCache is a cache that interfaces with Redis.
type RedisCache struct {
	client *redis.Client
}

// NewRedisCache creates a new Redis Cache.
func NewRedisCache() *RedisCache {
	c := &RedisCache{}

	c.client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := c.client.Ping().Result()
	if err != nil {
		panic(err)
	}

	return c
}

// Set sets a key value.
func (c *RedisCache) Set(key string, value interface{}, expiration time.Duration) error {
	err := c.client.Set(key, value, expiration).Err()
	if err != nil {
		return err
	}

	return nil
}

// Get returns the value of the given key.
func (c *RedisCache) Get(key string) (interface{}, bool, error) {
	val, err := c.client.Get(key).Result()
	if err == redis.Nil {
		return nil, false, nil
	} else if err != nil {
		return nil, false, err
	}

	return val, true, nil
}

// Close closes the cache.
func (c *RedisCache) Close() {
	c.client.Close()
}
