package cache

import "time"

// Cache stores a key value.
type Cache interface {
	Set(key string, value interface{}, expiration time.Duration) error
	Get(key string) (interface{}, bool, error)
	Close()
}
