package cache_test

import (
	"cache"
	"testing"
	"time"
)

func cacheTest(c cache.Cache, t *testing.T) {
	c.Set("hola", "que tal", 5*time.Second)
	value, ok, err := c.Get("hola")
	if err != nil {
		t.Errorf("error in Get %v", err)
	}
	if !ok {
		t.Error("could not Get, expected ok and got !ok")
	}

	if value != "que tal" {
		t.Errorf("expected 'que tal' and got %s", value)
	}

	_, ok, err = c.Get("holax")
	if err != nil {
		t.Errorf("error in Get %v", err)
	}
	if ok {
		t.Error("could not Get, expected ok and got !ok")
	}
}

func TestMock(t *testing.T) {
	c := cache.NewMockCache()
	defer c.Close()
	cacheTest(c, t)
}

func TestRedis(t *testing.T) {
	c := cache.NewRedisCache()
	defer c.Close()
	cacheTest(c, t)
}
