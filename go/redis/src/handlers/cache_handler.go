package handlers

import (
	"cache"
	"net/http"
	"strings"
	"time"
)

// CacheHandler is a cache server.
type CacheHandler struct {
	cache cache.Cache
}

// NewCacheHandler creates a new cache server.
func NewCacheHandler(cache cache.Cache) *CacheHandler {
	s := &CacheHandler{
		cache: cache,
	}

	return s
}

// ServeHTTP serves the request.
func (s *CacheHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")

	action := parts[1]
	if action != "set" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	key := "go:main:" + parts[2]
	value := parts[3]

	err := s.cache.Set(key, value, 60*time.Second)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
}
