package handlers_test

import (
	"cache"
	"fmt"
	"handlers"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCacheServer(t *testing.T) {
	cache := cache.NewMockCache()

	handler := handlers.NewCacheHandler(cache)
	rr := httptest.NewRecorder()

	key := "vale"
	value := "bien"

	req, err := http.NewRequest("GET", fmt.Sprintf("/set/%s/%s", key, value), nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("could not server HTTP code %v", rr.Code)
	}

	getValue, ok, err := cache.Get(key)
	fmt.Println(ok)
	if err != nil {
		t.Errorf("could not get '%v', error %v", key, err)
	} else if !ok {
		t.Errorf("could not get '%v'", key)
	} else if getValue != value {
		t.Errorf("expected '%v' and got '%v'", value, getValue)
	}
}

func TestCacheServerDetectsNotFound(t *testing.T) {
	cache := cache.NewMockCache()

	handler := handlers.NewCacheHandler(cache)
	rr := httptest.NewRecorder()

	req, err := http.NewRequest("GET", "/other/a/b", nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusNotFound {
		t.Errorf("expected StatusNotFound and got %v", rr.Code)
	}
}
