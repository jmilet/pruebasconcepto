package main

import (
	"cache"
	"handlers"
	"log"
	"net/http"
)

func main() {
	cache := cache.NewRedisCache()
	handler := handlers.NewCacheHandler(cache)
	http.Handle("set", handler)
	log.Fatal(http.ListenAndServe(":8080", handler))
}
