package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func processData(id int, input chan []byte) {
	type data struct {
		Value string `json:"value"`
	}

	var d data

	for {
		value := <-input
		err := json.Unmarshal(value, &d)
		if err != nil {
			panic(err)
		}

		log.Println(string(value))
		time.Sleep(2 * time.Second)
		log.Printf("[%d] => %s", id, d.Value)
	}
}

func foo(input chan<- []byte, w http.ResponseWriter, r *http.Request) {
	value, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	input <- value
}

func main() {
	input := make(chan []byte, 1000000)

	http.HandleFunc("/foo", func(w http.ResponseWriter, r *http.Request) {
		foo(input, w, r)
	})

	for i := 0; i < 200000; i++ {
		go processData(i, input)
	}

	log.Fatal(http.ListenAndServe(":8080", nil))
}
