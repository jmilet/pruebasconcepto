//
//  NETSerUsuario.h
//  PruebaSerializacion
//
//  Created by Juan Miguel Mora Carrera on 05/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NETSerUsuario : NSObject <NSCoding>

@property (strong, nonatomic) NSString *nombre;
@property (strong, nonatomic) NSString *contrasenya;

@end
