//
//  NETAppDelegate.h
//  PruebaSerializacion
//
//  Created by Juan Miguel Mora Carrera on 05/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NETAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
