//
//  NETViewController.m
//  PruebaSerializacion
//
//  Created by Juan Miguel Mora Carrera on 05/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "NETViewController.h"
#import "NETSerUsuario.h"

@interface NETViewController ()

@end

@implementation NETViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = paths[0];
    [self etiqueta].text = documentDir;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self nombreFichero]]){
        NETSerUsuario *usuario = [NSKeyedUnarchiver unarchiveObjectWithFile:[self nombreFichero]];
        UIAlertView *message = [[UIAlertView alloc]
                            initWithTitle:@"Datos del fichero"
                            message:[NSString stringWithFormat:@"%@-%@", usuario.nombre, usuario.contrasenya]
                            delegate:nil
                            cancelButtonTitle:@"OK"
                            otherButtonTitles:nil];
        [message show];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)grabar:(id)sender {
    
    NETSerUsuario *usuario = [[NETSerUsuario alloc] init];
    
    usuario.nombre = @"juan";
    usuario.contrasenya = @"la contrasenya";
    
    NSString *fichero = [self nombreFichero];
    
    [NSKeyedArchiver archiveRootObject:usuario toFile:fichero];
}

- (NSString*) nombreFichero{
    
    // Hay que notar que nos estamos apoyando en el valor que tiene el control.
    // No es elegante, pero para nuestra prueba es suficiente.
    
    NSString *path = [self etiqueta].text;
    NSString *fichero = [path stringByAppendingPathComponent:@"fichero.dat"];

    return fichero;
}

@end
