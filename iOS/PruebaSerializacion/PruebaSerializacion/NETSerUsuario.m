//
//  NETSerUsuario.m
//  PruebaSerializacion
//
//  Created by Juan Miguel Mora Carrera on 05/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "NETSerUsuario.h"

@implementation NETSerUsuario

@synthesize nombre, contrasenya;

- (void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:nombre forKey:@"nombre"];
    [aCoder encodeObject:contrasenya forKey:@"contrasenya"];
}

- (id) initWithCoder:(NSCoder *)aDecoder{
    if ( self = [super init]){
        nombre = [aDecoder decodeObjectForKey:@"nombre"];
        contrasenya = [aDecoder decodeObjectForKey:@"contrasenya"];
    }
    
    return self;
}

@end
