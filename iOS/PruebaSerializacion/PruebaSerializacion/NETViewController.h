//
//  NETViewController.h
//  PruebaSerializacion
//
//  Created by Juan Miguel Mora Carrera on 05/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NETViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *etiqueta;
- (IBAction)grabar:(id)sender;

@end
