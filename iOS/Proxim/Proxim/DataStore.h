//
//  DataStore.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 01/09/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRUser;

@interface DataStore : NSObject

@property (nonatomic, strong) PRUser* me;

+(id) getSharedInstance;

@end
