//
//  FriendsManagerDummy.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "FriendsManager.h"

@interface FriendsManagerDummy : FriendsManager <NSStreamDelegate>
@end


