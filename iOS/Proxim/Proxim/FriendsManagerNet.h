//
//  FriendsManagerNet.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 28/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "FriendsManager.h"

@interface FriendsManagerNet : FriendsManager <NSStreamDelegate> {
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    long _secuencia;
    NSMutableDictionary* responseBlocksDict;
}

@end
