//
//  FriendsManagerNet.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 28/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "FriendsManagerNet.h"
#import "PRUser.h"

CLLocationCoordinate2D nilLocation;

@implementation FriendsManagerNet

static FriendsManagerNet* instance = nil;

-(id) init
{
    self = [super init];
    
    if(self){
        
        CFReadStreamRef readStream;
        CFWriteStreamRef writeStream;
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"178.79.184.252", 9999, &readStream, &writeStream);
        inputStream = (__bridge NSInputStream *)readStream;
        outputStream = (__bridge NSOutputStream *)writeStream;
        
        [inputStream setDelegate:self];
        [outputStream setDelegate:self];
        
        [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        [inputStream open];
        [outputStream open];
        
        _secuencia = 0;
        responseBlocksDict = [[NSMutableDictionary alloc]init];
        
    }
    
    return self;
}
-(void) REQUEST_usersNearBy:(CLLocationCoordinate2D)coordinate completionBlock:(void(^)(NSArray* usersNearBy, NSError* error))completionBlock
{
    NSError* error;
    
    NSString* longitud = [NSString stringWithFormat:@"%f", coordinate.longitude];
    NSString* latitud = [NSString stringWithFormat:@"%f", coordinate.latitude];
    
    NSString* keySecuencia = [self newKeySecuencia];
    
    /* ==================== */
    /* Creación objeto JSON */
    /* ==================== */
    NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
    
    [data setObject:@"users_near_by" forKey:@"command"];
    [data setObject:keySecuencia forKey:@"command_seq"];
    [data setObject:@"pepe" forKey:@"user_id"];
    [data setObject:longitud forKey:@"longitud"];
    [data setObject:latitud forKey:@"latitud"];
    [data setObject:[[NSDate date] description] forKey:@"timestamp"];
    

    /* ============== */
    /* Envío del JSON */
    /* ============== */
    NSData* dataToSend = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];

    NSLog(@"%@", [[NSString alloc] initWithBytes:[dataToSend bytes] length:[dataToSend length] encoding:NSUTF8StringEncoding]);
    
    [outputStream write:(const uint8_t*)[dataToSend bytes] maxLength:[dataToSend length]];
    [outputStream write:(const uint8_t*) "\n" maxLength:1];

    /* =================== */
    /* Gestión del retorno */
    /* =================== */
    
    void(^copyCompletionBlock)(NSArray* usersNearBy, NSError* error)  = [completionBlock copy];
    
    void(^responseBlock)(NSDictionary* jsonDictObject) = ^(NSDictionary* jsonDictObject){
        NSMutableArray* usersNearBy = [NSMutableArray array];
    
        for(NSDictionary* user in [jsonDictObject objectForKey:@"user_list"]){
            [usersNearBy addObject:[PRUser userFromDict:user]];
        }
    
        // =========================================
        // Bloque a ejecutar al completar el comando
        // =========================================
        copyCompletionBlock(usersNearBy, nil);
    };
    
    [self saveResponseBlock:[responseBlock copy] keySecuencia:keySecuencia];
    
}

-(void) REQUEST_disconnectWithCompletionBlock:(void(^)(NSError* error))completionBlock
{
    NSError* error;
    
    NSString* keySecuencia = [self newKeySecuencia];
    
    /* ==================== */
    /* Creación objeto JSON */
    /* ==================== */
    NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
    
    [data setObject:@"disconnect" forKey:@"command"];
    [data setObject:keySecuencia forKey:@"command_seq"];
    [data setObject:@"pepe" forKey:@"user_id"];
    [data setObject:[[NSDate date] description] forKey:@"timestamp"];

    /* ============== */
    /* Envío del JSON */
    /* ============== */
    NSData* dataToSend = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];

    NSLog(@"%@", [[NSString alloc] initWithBytes:[dataToSend bytes] length:[dataToSend length] encoding:NSUTF8StringEncoding]);
    
    [outputStream write:(const uint8_t*)[dataToSend bytes] maxLength:[dataToSend length]];
    [outputStream write:(const uint8_t*) "\n" maxLength:1];


    /* =================== */
    /* Gestión del retorno */
    /* =================== */
    
    void(^copyCompletionBlock)(NSError* error)  = [completionBlock copy];
    
    void(^responseBlock)(NSDictionary* jsonDictObject) = ^(NSDictionary* jsonDictObject){
        /* ========================================= */
        /* Bloque a ejecutar al completar el comando */
        /* ========================================= */
        copyCompletionBlock(nil);
    };
    
    [self saveResponseBlock:[responseBlock copy] keySecuencia:keySecuencia];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
            
        case NSStreamEventHasBytesAvailable:
            NSLog(@"has available");
            if (theStream == inputStream) {
                
                NSLog(@"reading");
                
                NSError* error;
                uint8_t buffer[1024 * 32];
                int len;
                NSString *nsBuffer = nil;
                NSMutableData* nsData = [NSMutableData data];
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        nsBuffer = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        if (nil != nsBuffer) {
                            NSLog(@"server said: %@", nsBuffer);
                            [nsData appendBytes:buffer length:len];
                        }
                    }
                    
                    /* =================== */
                    /* Gestión del retorno */
                    /* =================== */
                    
                    NSArray* comandos = [nsBuffer componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                    
                    for (NSString* comando in comandos) {
                        
                        if ([comando length] == 0) {
                            continue;
                        }
                        
                        NSDictionary* jsonDictObject = [NSJSONSerialization JSONObjectWithData:[comando dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
                        if (error) {
                            NSLog(@"Error--------");
                        }
                        
                        NSLog(@"%@", responseBlocksDict);
                        NSLog(@"###%@###", jsonDictObject);
                        
                        NSString* keySecuencia = [jsonDictObject objectForKey:@"command_seq"];
                        
                        NSLog(@">>>%@<<<", keySecuencia);
                        
                        void(^bloque)(NSDictionary*) = [self getResponseBlockForKey:keySecuencia];
                        NSLog(@"%@", bloque);
                        
                        bloque(jsonDictObject);
                        
                        [self removeResponseBlockForKey:keySecuencia];
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            NSLog(@"End of stream!");

            break;
            
        default:
            NSLog(@"Unknown event %d", streamEvent);
    }
    
}

-(void) close
{
    [inputStream close];
    [outputStream close];
    instance = nil;
}

-(void) saveResponseBlock:(void(^)(NSDictionary* jsonDictObject))block keySecuencia:(NSString*)keySecuencia
{
    [responseBlocksDict setObject:block forKey:keySecuencia];
}

-(void(^)(NSDictionary*)) getResponseBlockForKey:(NSString*) key
{
    return [responseBlocksDict objectForKey:key];
}

-(long) newSecuencia
{
    long retorno = _secuencia;
    
    _secuencia++;
    
    return retorno;
}

-(NSString*) newKeySecuencia
{
    return [NSString stringWithFormat:@"%ld", [self newSecuencia]];
}

-(void)removeResponseBlockForKey:keySecuencia
{
    [responseBlocksDict removeObjectForKey:keySecuencia];
}

@end
