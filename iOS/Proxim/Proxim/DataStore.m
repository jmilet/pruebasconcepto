//
//  DataStore.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 01/09/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "DataStore.h"

static DataStore* instance = nil;

@implementation DataStore


+(id) getSharedInstance
{
    if (instance == nil) {
        instance = [self alloc];
    }
    
    return instance;
}



@end
