//
//  FriendsManager.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 01/09/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "FriendsManager.h"
#import "FriendsManagerDummy.h"
#import "FriendsManagerNet.h"


static id instance = nil;

@implementation FriendsManager

+(id) getSharedInstance
{
    if (instance == nil){
        instance = [[FriendsManagerDummy alloc] init];
    }
    
    return instance;
}

-(void) REQUEST_usersNearBy:(CLLocationCoordinate2D)coordinate completionBlock:(void(^)(NSArray* usersNearBy, NSError* error))completionBlock
{
}

-(void) REQUEST_disconnectWithCompletionBlock:(void(^)(NSError* error))completionBlock
{
}

-(void) close
{
}

@end
