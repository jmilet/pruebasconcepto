//
//  FriendsManager.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 01/09/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface FriendsManager : NSObject

+(id) getSharedInstance;
-(void) REQUEST_usersNearBy:(CLLocationCoordinate2D)coordinate completionBlock:(void(^)(NSArray* usersNearBy, NSError* error))completionBlock;
-(void) REQUEST_disconnectWithCompletionBlock:(void(^)(NSError* error))completionBlock;
-(void) close;

@end
