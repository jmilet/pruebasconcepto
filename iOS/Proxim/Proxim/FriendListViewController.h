//
//  FriendListViewController.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class FriendsManager;


@interface FriendListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate> {

    FriendsManager* _friendsManager;
    CLLocationManager* _locationManager;
    NSMutableArray* _friends;
    UIActivityIndicatorView* _activityIndicator;
    UIBarButtonItem* _bbiRefresh;
    UIBarButtonItem* _bbiActivityIndicator;
}

@end
