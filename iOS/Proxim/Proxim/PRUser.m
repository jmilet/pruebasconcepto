//
//  PRUser.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 24/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "PRUser.h"

@implementation PRUser

@synthesize userName, coordinate;


- (id) initWithUserName:(NSString*)anUserName coordinate:(CLLocationCoordinate2D)aCoordinate {
    
    self = [super init];
    
    if (self) {
        [self setUserName:anUserName];
        [self setCoordinate:aCoordinate];
    }
    
    return self;
}

- (id) initWithUserName:(NSString*) name
{
    CLLocationCoordinate2D dummyCoordinate;
    return [self initWithUserName:name coordinate:dummyCoordinate];
}

+ (id) userFromDict:(NSDictionary*)dictionary
{
    id instance;
    instance = [self alloc];
    
    if(instance){
        NSString* userName = [dictionary objectForKey:@"user_id"];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[dictionary objectForKey:@"latitud"] floatValue];
        coordinate.longitude = [[dictionary objectForKey:@"longitud"] floatValue];
        
        instance = [instance initWithUserName:userName coordinate:coordinate];
    }
    
    return instance;
}

-(id) description
{
    return [NSString stringWithFormat:@"%@ - %f, %f", self.userName, self.coordinate.longitude, self.coordinate.latitude];
}

@end
