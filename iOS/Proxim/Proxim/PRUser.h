//
//  PRUser.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 24/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PRUser : NSObject

@property (nonatomic, strong) NSString* userName;
@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id) initWithUserName:(NSString*)userName coordinate:(CLLocationCoordinate2D) coordinate;
- (id) initWithUserName:(NSString*)userName;
+ (id) userFromDict:(NSDictionary*)dictionary;

@end

