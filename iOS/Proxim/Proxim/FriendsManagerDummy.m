//
//  FriendsManagerDummy.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "FriendsManagerDummy.h"
#import "PRUser.h"



@implementation FriendsManagerDummy



-(id) init
{
    self = [super init];
    
    if(self){
    }
    
    return self;
}

-(void) REQUEST_usersNearBy:(CLLocationCoordinate2D)coordinate completionBlock:(void(^)(NSArray* usersNearBy, NSError* error))completionBlock
{
    void(^copyCompletionBlock)(NSArray* usersNearBy, NSError* error)  = [completionBlock copy];
    
    void(^responseBlock)(NSDictionary* jsonDictObject) = ^(NSDictionary* jsonDictObject){
        NSMutableArray* usersNearBy = [NSMutableArray array];
        
        CLLocationCoordinate2D coordinate;
        
        coordinate.latitude = 41.373320;
        coordinate.longitude = 2.058133;
        
        [usersNearBy addObject:[[PRUser alloc]initWithUserName:@"juan" coordinate:coordinate]];
        // =========================================
        // Bloque a ejecutar al completar el comando
        // =========================================
        copyCompletionBlock(usersNearBy, nil);
    };
    
    responseBlock(nil);
    
}

-(void) REQUEST_disconnectWithCompletionBlock:(void(^)(NSError* error))completionBlock
{
    /* =================== */
    /* Gestión del retorno */
    /* =================== */
    
    void(^copyCompletionBlock)(NSError* error)  = [completionBlock copy];
    
    void(^responseBlock)(NSDictionary* jsonDictObject) = ^(NSDictionary* jsonDictObject){
        /* ========================================= */
        /* Bloque a ejecutar al completar el comando */
        /* ========================================= */
        copyCompletionBlock(nil);
    };
    
    responseBlock(nil);
}

-(void) close
{
}


@end
