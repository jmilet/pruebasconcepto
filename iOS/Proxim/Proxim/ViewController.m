//
//  ViewController.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "ViewController.h"
#import "FriendListViewController.h"
#import "FriendsManager.h"
#import "DataStore.h"
#import "PRUser.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    NSString* nick = [[NSUserDefaults standardUserDefaults] objectForKey:@"nick"];
    if (nick){
        [nickText setText:nick];
    }
    
    [[self navigationItem] setTitle:[self title]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [nickText resignFirstResponder];
    
    [self saveNick: [textField text]];
    
    return YES;
}

- (IBAction)connect:(id)sender {
    FriendListViewController *friendListViewController = [[FriendListViewController alloc] init];
    
    [[DataStore getSharedInstance] setMe: [[PRUser alloc] initWithUserName:[nickText text]]];
    
    [[self navigationController] pushViewController:friendListViewController animated:YES];
}

- (void)saveNick:(NSString *)nick {
    [[NSUserDefaults standardUserDefaults] setObject:nick forKey:@"nick"];
}

-(NSString*)title
{
    return @"Login";
}

@end
