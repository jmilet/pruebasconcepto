//
//  AppDelegate.h
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
