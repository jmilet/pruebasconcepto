//
//  FriendListViewController.m
//  Proxim
//
//  Created by Juan Miguel Mora Carrera on 07/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "FriendListViewController.h"
#import "FriendsManager.h"
#import "PRUser.h"
#import "DataStore.h"

@interface FriendListViewController ()

@end

@implementation FriendListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    
    NSLog(@"initWithNibName:bundle");
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _friends = [[NSMutableArray alloc] init];
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
        [_locationManager setDistanceFilter:kCLDistanceFilterNone];
        [_locationManager setActivityType:CLActivityTypeFitness];
        [_locationManager startUpdatingLocation];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"viewDidLoad");
    
    [[self navigationItem] setTitle:[self title]];
    
    _bbiRefresh = [[UIBarButtonItem alloc]
                   initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                   target:self
                   action:@selector(refrescar:)];
    [[self navigationItem] setRightBarButtonItem:_bbiRefresh];
    
    
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicator.hidesWhenStopped = YES;
    _bbiActivityIndicator = [[UIBarButtonItem alloc] initWithCustomView:_activityIndicator];

    _friendsManager = [FriendsManager getSharedInstance];
    
    [self loadFriends];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    
    if (!cell){
        cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"UITableViewCell"];
    }
    
    PRUser* user = [_friends objectAtIndex:indexPath.row];
    //cell.textLabel.text = [NSString stringWithFormat:@"%@ %f, %f", [user userName], user.coordinate.longitude, user.coordinate.latitude];
    
    CLLocationCoordinate2D misCoordenadas;
    
    misCoordenadas = [[[DataStore getSharedInstance] me] coordinate];
    CLLocation* milocation = [[CLLocation alloc] initWithLatitude:misCoordenadas.latitude longitude:misCoordenadas.longitude];

    CLLocation* suLocation = [[CLLocation alloc]initWithLatitude:user.coordinate.latitude longitude:user.coordinate.longitude];
    
    CLLocationDistance distance = [milocation distanceFromLocation:suLocation];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %.2f metros", user.userName, distance];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [[_friendsManager friendsNearBy:nil] count];
    if (!_friends){
        return 0;
    }
    else {
        return [_friends count];
    }
}

- (IBAction)refrescar:(id)sender {
    [self loadFriends];
}

-(void) loadFriends
{
    [_activityIndicator startAnimating];
    [[self navigationItem] setRightBarButtonItem:_bbiActivityIndicator];

    [_friendsManager REQUEST_usersNearBy: [[[DataStore getSharedInstance] me] coordinate]
                        completionBlock:^(NSArray* friends, NSError* error){
                            [_friends removeAllObjects];
                            [_friends addObjectsFromArray:friends];
                            [[self tableView] reloadData];
                            [_activityIndicator stopAnimating];
                            [[self navigationItem] setRightBarButtonItem:_bbiRefresh];
   }];
}

-(NSString*)title
{
    return @"Usuarios Conectados";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d", [indexPath indexAtPosition:0]);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"update location");
    //[_locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    
    NSLog(@"%f %f", location.coordinate.longitude, location.coordinate.latitude);
    
    [[[DataStore getSharedInstance] me] setCoordinate:location.coordinate];
    
    [self loadFriends];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_locationManager stopUpdatingLocation];
}

@end
