//
//  ViewController.m
//  TestNavigationControlerNoSegues
//
//  Created by Juan Miguel Mora Carrera on 06/08/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)boton:(id)sender {
    ViewController2 *vc = [[ViewController2 alloc] init];
    
    [vc setTexto:@"hola que tal"];
    
    [self presentViewController:vc animated:YES completion:nil];

}

@end
