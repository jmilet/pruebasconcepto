//
//  main.m
//  TestBlocks
//
//  Created by Juan Miguel Mora Carrera on 22/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

void funcion(void (^bloque)(int x)){
    bloque(5);
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSLog(@"Hello, World!");
        
        int (^bloque)(int x);
        bloque = ^(int x) { return x + 1; };
        NSLog(@"%d", bloque(2));
        
        void (^bloque2)(void) = ^(void){
            NSLog(@"ok");
        };
        bloque2();
        
        funcion(^(int x){
            NSLog(@"%d", x + 1);
        });
        
    }
    return 0;
}

