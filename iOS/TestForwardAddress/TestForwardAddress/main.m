//
//  main.m
//  TestForwardAddress
//
//  Created by Juan Miguel Mora Carrera on 31/10/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TestAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TestAppDelegate class]));
    }
}
