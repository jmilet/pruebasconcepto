//
//  TestViewController.h
//  TestForwardAddress
//
//  Created by Juan Miguel Mora Carrera on 31/10/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *mensaje;
@property (weak, nonatomic) IBOutlet UITextField *direccion;
@property (weak, nonatomic) IBOutlet UIButton *buscar;
@end
