//
//  TestViewController.m
//  TestForwardAddress
//
//  Created by Juan Miguel Mora Carrera on 31/10/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import "TestViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface TestViewController ()

@end

@implementation TestViewController

@synthesize buscar, direccion, mensaje;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buscar:(id)sender {
    NSString *address = [direccion text];
    
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    
    [geocoder geocodeAddressString:address
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     NSLog(@"--> %d", [placemarks count]);
                     
                     if (!error){
                         for (CLPlacemark* aPlacemark in placemarks)
                         {
                             CLLocation *locationEach = [aPlacemark location];
                             NSLog(@"%@", locationEach);
                             [mensaje setText:[locationEach description]];
                             
                         }
                     } else {
                         NSLog(@"Geocoding error: %@", [error localizedDescription]);
                     }
                 }];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *) tf
{
    NSLog(@"OK");
    [tf resignFirstResponder]; // Renuncia a ser el primer respondedor.
    return YES; // Informa de que ha de volver y por lo tanto cerrar el teclado.
}

@end
