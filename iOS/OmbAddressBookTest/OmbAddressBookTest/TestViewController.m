//
//  TestViewController.m
//  OmbAddressBookTest
//
//  Created by Juan Miguel Mora Carrera on 14/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
