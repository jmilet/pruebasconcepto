//
//  NETMasterViewController.h
//  Network
//
//  Created by Juan Miguel Mora Carrera on 03/06/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NETMasterViewController : UITableViewController <NSStreamDelegate> {

    NSInputStream *inputStream;
    NSOutputStream *outputStream;
}

-(void) cargarInfo;

@end
