//
//  NETDetailViewController.m
//  Network
//
//  Created by Juan Miguel Mora Carrera on 03/06/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "NETDetailViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface NETDetailViewController () <CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}
- (void)configureView;

@end

@implementation NETDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 0;
    NSLog(@"view did load");

    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)posicionGPS:(id)sender {
    NSLog(@"press");
    [locationManager startUpdatingLocation];
}

- (IBAction)camara:(id)sender {
    [self startCameraControllerFromViewController:self usingDelegate:self];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    self.detailDescriptionLabel.text = [[NSString alloc] initWithFormat:@"%f, %f", location.coordinate.latitude, location.coordinate.longitude];
    
    MKPointAnnotation *pointAnotation = [[MKPointAnnotation alloc] init];
    pointAnotation.coordinate = location.coordinate;
    pointAnotation.title = @"mobilers";
    [[self mapView] addAnnotation:pointAnotation];
    
    MKCoordinateRegion cr;
    cr.center = pointAnotation.coordinate;
    cr.span.latitudeDelta = 20;
    cr.span.longitudeDelta = 20;
    [[self mapView] setRegion:cr animated:TRUE];
    
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error
{
    NSLog(@"error");
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"window will appear");
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    // Displays a control that allows the user to choose picture or

    // movie capture, if both are available:
    cameraUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeCamera];
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = delegate;
    [self presentViewController: cameraUI animated: YES completion: NULL];
    return YES;
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    NSLog(@"test");
    [self dismissViewControllerAnimated: YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        

            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Code here to support video if enabled
    }
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}
@end
