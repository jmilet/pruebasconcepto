//
//  NETDetailViewController.h
//  Network
//
//  Created by Juan Miguel Mora Carrera on 03/06/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface NETDetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)posicionGPS:(id)sender;
- (IBAction)camara:(id)sender;
@end
