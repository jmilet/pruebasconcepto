//
//  BirdsAppDelegate.h
//  BirdSighting
//
//  Created by Juan Miguel Mora Carrera on 21/08/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirdsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
