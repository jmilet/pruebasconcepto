//
//  BirdSightingDataController.h
//  BirdWatching
//
//  Created by Juan Miguel Mora Carrera on 21/08/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BirdSighting;

@interface BirdSightingDataController : NSObject

@property (nonatomic, copy) NSMutableArray *masterBirdSightingList;

- (NSUInteger)countOfList;
- (BirdSighting *)objectInListAtIndex:(NSUInteger)theIndex;
- (void)addBirdSightingWithSighting:(BirdSighting *)sighting;

@end
