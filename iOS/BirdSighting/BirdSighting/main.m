//
//  main.m
//  BirdSighting
//
//  Created by Juan Miguel Mora Carrera on 21/08/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BirdsAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BirdsAppDelegate class]));
    }
}
