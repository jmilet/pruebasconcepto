gcc -g -o client client.m \
-I `gnustep-config --variable=GNUSTEP_SYSTEM_HEADERS` \
-L `gnustep-config --variable=GNUSTEP_SYSTEM_LIBRARIES` \
-lgnustep-base -fconstant-string-class=NSConstantString \
-D_NATIVE_OBJC_EXCEPTIONS

gcc -g -o server server.m \
-I `gnustep-config --variable=GNUSTEP_SYSTEM_HEADERS` \
-L `gnustep-config --variable=GNUSTEP_SYSTEM_LIBRARIES` \
-lgnustep-base -fconstant-string-class=NSConstantString \
-D_NATIVE_OBJC_EXCEPTIONS
