#include <stdio.h>
#import <Foundation/Foundation.h>

@interface Server : NSObject
-(int) foo;
-(void) imprimir:(id)obj;
@end

@implementation Server

-(int) foo{
	NSLog(@"Funciona");
	return 10;
}

-(void) imprimir:(id)obj{
	NSLog(@"----------------- %@", [obj valor]);
	[obj display];
}

@end



int main(void){
	NSConnection *theConnection;
	Server *serverObject;

	CREATE_AUTORELEASE_POOL(pool);

	NSSocketPort *port = [NSSocketPort port];


	serverObject = [[Server alloc] autorelease];
	[serverObject foo];



	theConnection = [NSConnection connectionWithReceivePort: port
							  sendPort: port];


	[theConnection setRootObject:serverObject];

 	if ([theConnection registerName:@"server" withNameServer: [NSSocketPortNameServer sharedInstance]] == NO) {
		NSLog(@"Error\n");
	}

	[[NSRunLoop currentRunLoop] run];

	RELEASE(pool);
	return 0;
}
