#include <stdio.h>
#import <Foundation/Foundation.h>


@interface TipoRaro : NSObject
        NSString *cadena;
	-(void) display;
	-(NSString*) valor;
@end

@implementation TipoRaro

-(id) init {
	[super init];
        cadena = @"OK";
}


-(void) display {
	NSLog(cadena);
}

-(NSString*) valor
{
	NSLog(@"Estoy en valor");
	return cadena;
}
@end

int main(void){
 	
  /* Create an instance of the NSAutoreleasePool class */
  CREATE_AUTORELEASE_POOL(pool);

  TipoRaro *r = [[[TipoRaro alloc] init] autorelease];

  /* Get the proxy */
   id proxy = [NSConnection
    rootProxyForConnectionWithRegisteredName:@"server"
    host:@"localhost"
    usingNameServer: [NSSocketPortNameServer sharedInstance]]; 
	
  /* The rest of your program code goes here */
  NSLog(@"%d", [proxy foo]);
  [proxy imprimir: r];

  while(1);

  /* Release the pool */
  RELEASE(pool);
}
