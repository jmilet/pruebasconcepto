//
//  ViewController.h
//  TestView
//
//  Created by Juan Miguel Mora Carrera on 19/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *boton;

@end
