//
//  ViewController2.m
//  TestView
//
//  Created by Juan Miguel Mora Carrera on 19/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "ViewController2.h"

@implementation ViewController2

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)botonPulsado:(id)sender {
    [self.view removeFromSuperview];
}

@end
