//
//  ViewController.m
//  TestView
//
//  Created by Juan Miguel Mora Carrera on 19/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"

@interface ViewController ()
    @property (strong, nonatomic) ViewController2 *secondViewController;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)botonPulsado:(id)sender {
    self.secondViewController = [[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil];
    [self.view addSubview:self.secondViewController.view];
}


@end
