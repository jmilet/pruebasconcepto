//
//  NETDetailViewController.h
//  TestBasicoStoryboards
//
//  Created by Juan Miguel Mora Carrera on 21/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NETDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
