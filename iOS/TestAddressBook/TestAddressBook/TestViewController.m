//
//  TestViewController.m
//  TestAddressBook
//
//  Created by Juan Miguel Mora Carrera on 14/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import "TestViewController.h"
#import "AddressBook/AddressBook.h"

@interface TestViewController ()

@end

@implementation TestViewController

@synthesize data;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    while(1) mostrarAddressBook(data);
 
}

void mostrarAddressBook(CFArrayRef data){
    CFErrorRef error;
    ABAddressBookRef ab = ABAddressBookCreateWithOptions(NULL, &error);
    data = ABAddressBookCopyArrayOfAllPeople(ab);
    int cuantos = ABAddressBookGetPersonCount(ab);
    
    for (int i = 0; i < cuantos; i++){
        ABRecordRef record = CFArrayGetValueAtIndex(data, i);
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", ABRecordCopyValue(record, kABPersonFirstNameProperty), ABRecordCopyValue(record, kABPersonLastNameProperty)];
        //NSLog(@"%@", fullName);
        //CFRelease(record);
    }

    CFRelease(data);
    CFRelease(ab);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
