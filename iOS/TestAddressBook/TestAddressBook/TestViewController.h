//
//  TestViewController.h
//  TestAddressBook
//
//  Created by Juan Miguel Mora Carrera on 14/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressBook/AddressBook.h"

@interface TestViewController : UITableViewController

@property (nonatomic) CFArrayRef data;

@end
