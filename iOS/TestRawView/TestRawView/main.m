//
//  main.m
//  TestRawView
//
//  Created by Juan Miguel Mora Carrera on 20/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NETAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NETAppDelegate class]));
    }
}
