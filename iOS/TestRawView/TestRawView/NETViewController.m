//
//  NETViewController.m
//  TestRawView
//
//  Created by Juan Miguel Mora Carrera on 20/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "NETViewController.h"


@interface NETViewController ()
@property (strong, nonatomic) UIView *unaView;
@property int diferenciaAncho;
@property int veces;
@end

@implementation NETViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // La vista básica.
    CGRect aFrame = CGRectMake(10, 20, 30, 40);
    self.unaView = [[UIView alloc] initWithFrame:aFrame];
    self.unaView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:self.unaView];

    // Botón más ancho.
    UIButton *unBotonMasAncho = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    unBotonMasAncho.frame = CGRectMake(150, 80, 120, 30);
    [unBotonMasAncho setTitle:@"Más ancho" forState:UIControlStateNormal];
    [unBotonMasAncho setTitle:@"Ooops" forState:UIControlStateHighlighted];
    
    [unBotonMasAncho addTarget:self action:@selector(unBotonMasAnchoPulsado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:unBotonMasAncho];

    // Botón más estrecho.
    UIButton *unBotonMasEstrecho = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    unBotonMasEstrecho.frame = CGRectMake(30, 80, 120, 30);
    [unBotonMasEstrecho setTitle:@"Más estrecho" forState:UIControlStateNormal];
    [unBotonMasEstrecho setTitle:@"Ooops" forState:UIControlStateHighlighted];
    
    [unBotonMasEstrecho addTarget:self action:@selector(unBotonMasEstrechoPulsado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:unBotonMasEstrecho];
    
    // Configuración del avance del ancho de la vista.
    self.diferenciaAncho = 20;
    self.veces = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)unBotonMasAnchoPulsado
{
    CGRect unFrame = self.unaView.frame;
    if (self.veces < 10){
        unFrame.size.width += self.diferenciaAncho;
        self.veces++;
    }
    self.unaView.frame = unFrame;
}

-(void)unBotonMasEstrechoPulsado
{
    CGRect unFrame = self.unaView.frame;
    if (self.veces > 0){
        unFrame.size.width -= self.diferenciaAncho;
        self.veces--;
    }
    self.unaView.frame = unFrame;
}

@end
