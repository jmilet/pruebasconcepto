//
//  NETViewController.m
//  TestRawTextField
//
//  Created by Juan Miguel Mora Carrera on 20/07/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "NETViewController.h"

@interface NETViewController ()

@end

@implementation NETViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Text Field.
    unTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 20, 280, 50)];
    unTextField.borderStyle = UITextBorderStyleRoundedRect;
    unTextField.clearButtonMode = UITextFieldViewModeAlways; // Pone la cruz de borrado.
    unTextField.delegate = self; // Le decimos que somos nostros los que gestionamos los eventos del TextField.
    unTextField.placeholder = @"hola que tal";
    [self.view addSubview:unTextField];
    
    // Botón.
    unBoton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    unBoton.frame = CGRectMake(100, 150, 90, 50);
    [unBoton setTitle:@"Mostrar" forState:UIControlStateNormal];
    [unBoton addTarget:self action:@selector(unBotonPulsado) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:unBoton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldShouldBeginEditing");
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"Han pulsado enter");
    
    [unTextField resignFirstResponder];
    
    return YES;
}

- (void) unBotonPulsado
{
    NSLog(@"botón pulsado");
    
    [unTextField resignFirstResponder];
    
    UIAlertView *unaAlertView = [[UIAlertView alloc]
                                    initWithTitle:@"mensaje"
                                    message:unTextField.text
                                    delegate:nil
                                    cancelButtonTitle:@"OK"
                                    otherButtonTitles: nil];
    
    [unaAlertView show];

}

@end
