//
//  TestStatus.m
//  TestRest
//
//  Created by Juan Miguel Mora Carrera on 01/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import "TestStatus.h"

@implementation TestStatus

@synthesize statusID, createdAt, text, urlString, inReplyToScreenName, isFavorited, user;

@end
