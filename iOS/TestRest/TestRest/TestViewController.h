//
//  TestViewController.h
//  TestRest
//
//  Created by Juan Miguel Mora Carrera on 01/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@interface TestViewController : UIViewController <RKObjectLoaderDelegate> {
    RKObjectManager *manager;
}

@end
