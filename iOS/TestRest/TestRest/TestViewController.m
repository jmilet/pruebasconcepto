//
//  TestViewController.m
//  TestRest
//
//  Created by Juan Miguel Mora Carrera on 01/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import "TestViewController.h"
#import "TestUser.h"
#import "TestStatus.h"

@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    manager = [RKObjectManager objectManagerWithBaseURLString:@"http://www.twitter.com"];
    manager.acceptMIMEType = RKMIMETypeJSON;
    manager.serializationMIMEType = RKMIMETypeJSON;
    
    [manager.client setValue:@"1234" forHTTPHeaderField:@"X-RESTKIT-API-KEY"];
    
    RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:[TestUser class]];
    [userMapping mapKeyPath:@"id" toAttribute:@"userID"];
    [userMapping mapKeyPath:@"screen_name" toAttribute:@"screenName"];
    [userMapping mapAttributes:@"name", nil];
    
    RKObjectMapping *statusMapping = [RKObjectMapping mappingForClass:[TestStatus class]];
    [statusMapping mapKeyPathsToAttributes:@"id", @"statusID",
     @"created_at", @"createdAt",
     @"text", @"text",
     @"url", @"urlString",
     @"in_reply_to_screen_name", @"inReplyToScreenName",
     @"favorited", @"isFavorited",
     nil];
    [statusMapping mapRelationship:@"user" withMapping:userMapping];
    
    [RKObjectMapping addDefaultDateFormatterForString:@"E MMM d HH:mm:ss Z y" inTimeZone:nil];
    
    [manager.mappingProvider setObjectMapping:statusMapping forResourcePathPattern:@"/status/user_timeline/:username"];
    [manager loadObjectsAtResourcePath:@"/status/user_timeline/RestKit" delegate:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects{
    NSLog(@"%@", objects);
    for (TestStatus *status in objects){
        NSLog(@"%@", status.text);
        NSLog(@"%@", status.createdAt);
    }
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
}

@end
