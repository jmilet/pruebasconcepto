//
//  TestUser.h
//  TestRest
//
//  Created by Juan Miguel Mora Carrera on 01/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestUser : NSObject

@property (nonatomic, retain) NSNumber *userID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *screenName;

@end
