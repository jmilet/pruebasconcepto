//
//  TestStatus.h
//  TestRest
//
//  Created by Juan Miguel Mora Carrera on 01/11/12.
//  Copyright (c) 2012 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestUser.h"

@interface TestStatus : NSObject

@property (nonatomic, retain) NSNumber *statusID;
@property (nonatomic, retain) NSDate *createdAt;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *urlString;
@property (nonatomic, retain) NSString *inReplyToScreenName;
@property (nonatomic, retain) NSNumber *isFavorited;
@property (nonatomic, retain) TestUser *user;

@end
