//
//  PRCConexionProceso.m
//  Procesos
//
//  Created by Juan Miguel Mora Carrera on 31/12/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//


#import "PRCConexionProceso.h"

@interface PRCConexionProceso ()

{
    CFReadStreamRef _readStream;
    CFWriteStreamRef _writeStream;
    BOOL _firsTime;
    NSIndexPath* _indexPath;
}
    @property (nonatomic, strong) NSInputStream *inputStream;
    @property (nonatomic, strong) NSOutputStream *outputStream;
@end



@implementation PRCConexionProceso

-(id) initWithDelegate:(id<PRCConexionProcesoProtocol>)delegate;
{
    self = [super init];
    
    if (self) {
        NSString *server = @"aplicarium.com";
        //NSString *server = @"192.168.1.36";
        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)server, 5678, &_readStream, &_writeStream);
        self.inputStream = (__bridge NSInputStream *)_readStream;
        self.outputStream = (__bridge NSOutputStream *)_writeStream;
        
        [self.inputStream setDelegate:self];
        [self.outputStream setDelegate:self];
        
        [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        [self.inputStream open];
        [self.outputStream open];
        
        self.delegate = delegate;
        _firsTime = YES;
    }
    
    return self;
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            //NSLog(@"Stream opened");
            break;
            
        case NSStreamEventHasBytesAvailable:
            //NSLog(@"has available");
            if (theStream == self.inputStream) {
                
                //NSLog(@"reading");
                
                uint8_t buffer[1024 * 32];
                int len;
                NSString *nsBuffer = nil;
                NSMutableData* nsData = [NSMutableData data];
                
                while ([self.inputStream hasBytesAvailable]) {
                    len = [self.inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        nsBuffer = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        
                        if (nil != nsBuffer) {
                            //NSLog(@"%@", nsBuffer);
                            [nsData appendBytes:buffer length:len];
                        }
                    }
                }
                
                //NSLog(@"%@", nsBuffer);
                
                if ([nsBuffer isEqualToString:@"finalizado\n"]) {
                    NSLog(@">>>%@", nsBuffer);
                    [self cerrar];
                    [self.delegate conexionesCerradasForConexion:self];
                }
                else {
                    [self.delegate datosActualizadosForConexion:self datos:nsBuffer];
                }
            }
            break;
            
        case NSStreamEventHasSpaceAvailable:
            if (theStream == self.outputStream && _firsTime) {
                NSData *data = [NSData dataWithBytes:"crear\r\n" length:7];
                    
                //NSLog(@"%s", [data bytes]);
                    
                [self.outputStream write: [data bytes] maxLength: [data length]];
                _firsTime = NO;
            }
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            NSLog(@"End of stream!");
            break;
        
        default:
            NSLog(@"Unknown event %d", streamEvent);
    }
    
}

-(void) finalizar
{
    NSData *data = [NSData dataWithBytes:"finalizar\r\n" length:11];
    [self.outputStream write:[data bytes] maxLength:[data length]];
}

-(void) cerrar
{
    [self.inputStream close];
    [self.outputStream close];
    CFWriteStreamClose(_writeStream);
    CFReadStreamClose(_readStream);
    CFRelease(_writeStream);
    CFRelease(_readStream);
}

@end
