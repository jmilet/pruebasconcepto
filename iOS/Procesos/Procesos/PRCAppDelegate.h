//
//  PRCAppDelegate.h
//  Procesos
//
//  Created by Juan Miguel Mora Carrera on 31/12/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
