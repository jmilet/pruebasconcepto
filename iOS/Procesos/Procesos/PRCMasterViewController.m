//
//  PRCMasterViewController.m
//  Procesos
//
//  Created by Juan Miguel Mora Carrera on 31/12/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import "PRCMasterViewController.h"
#import "PRCDetailViewController.h"
#import "PRCConexionProceso.h"

@interface PRCMasterViewController () {
    NSMutableArray *_objects;
    NSMutableArray *_conexiones;
}
@end

@implementation PRCMasterViewController

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (PRCDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    
    NSMutableString *mensaje =[[NSMutableString alloc] initWithString:@""];
    [_objects insertObject:mensaje atIndex:0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self newConexionConBufferMensaje:mensaje];
}

-(void) newConexionConBufferMensaje:(NSMutableString*)mensaje
{
    if(!_conexiones) {
        _conexiones = [[NSMutableArray alloc] init];
    }
    
    PRCConexionProceso *conexion = [[PRCConexionProceso alloc] initWithDelegate:self];
    [_conexiones insertObject:conexion atIndex:0];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDate *object = _objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[_conexiones objectAtIndex:indexPath.row] finalizar];
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *object = _objects[indexPath.row];
    self.detailViewController.detailItem = object;
}

-(void) datosActualizadosForConexion:(PRCConexionProceso *)conexion datos:(NSString *)datos
{
    int index = [_conexiones indexOfObject:conexion];
    
    if ([datos hasPrefix:@"iniciado"]) {
        NSArray *elementos = [datos componentsSeparatedByString:@"iniciado "];
        [[_objects objectAtIndex:index] setString:[elementos objectAtIndex:1]];
        [self.tableView reloadData];
    } else if ([datos hasPrefix:@"finalizado"]) {
        [_conexiones removeObject:conexion];
    }
    else {
        NSLog(@"%@", datos);
    }
}

-(void) conexionesCerradasForConexion:(PRCConexionProceso *)conexion
{
    int index = [_conexiones indexOfObject:conexion];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [_conexiones removeObject:conexion];
}

@end
