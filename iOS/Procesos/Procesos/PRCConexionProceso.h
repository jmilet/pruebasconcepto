//
//  PRCConexionProceso.h
//  Procesos
//
//  Created by Juan Miguel Mora Carrera on 31/12/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PRCConexionProcesoProtocol;

@interface PRCConexionProceso : NSObject <NSStreamDelegate>

@property (weak, nonatomic) id<PRCConexionProcesoProtocol> delegate;

-(id) initWithDelegate:(id<PRCConexionProcesoProtocol>)delegate;
-(void) finalizar;
@end

@protocol PRCConexionProcesoProtocol <NSObject>

-(void) datosActualizadosForConexion:(PRCConexionProceso*)conexion datos:(NSString*)datos;
-(void) conexionesCerradasForConexion:(PRCConexionProceso*)conexion;

@end
