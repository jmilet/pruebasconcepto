//
//  PRCMasterViewController.h
//  Procesos
//
//  Created by Juan Miguel Mora Carrera on 31/12/13.
//  Copyright (c) 2013 Juan Miguel Mora Carrera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRCConexionProceso.h"

@class PRCDetailViewController;

@interface PRCMasterViewController : UITableViewController <PRCConexionProcesoProtocol>

@property (strong, nonatomic) PRCDetailViewController *detailViewController;

-(void) newConexionConBufferMensaje:(NSMutableString*)mensaje;

@end
