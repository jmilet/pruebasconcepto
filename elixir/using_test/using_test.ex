defmodule Usado do
	
	defmacro __using__(_options) do
		quote do
			import unquote(__MODULE__)
			Module.register_attribute __MODULE__, :var_test, accumulate: true
			@before_compile unquote(__MODULE__)
		end
	end

	defmacro __before_compile__(_env) do
		quote do
			def run do
				IO.inspect @var_test
			end
		end
	end

	defmacro anyadir(valor)  do
		IO.write "ini: anyadir #{valor}"
		quote do
			@var_test unquote(valor)
		end
	end

end

defmodule Principal do
	use Usado

	anyadir :uno
	anyadir :dos
end
