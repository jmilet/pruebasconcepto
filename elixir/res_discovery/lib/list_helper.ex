defmodule ListHelper do
	def remove_duplicates(list) do
		Enum.reverse(_remove_duplicates(list, []))
	end

	defp _remove_duplicates([], acc) do
		acc
	end

	defp _remove_duplicates([h|t], acc) do
		_remove_duplicates(remove_elem(t, h), [h|acc])
	end

	def remove_elem(list, elem) do
		Enum.reverse(_remove_elem(list, elem, []))
	end

	defp _remove_elem([], _, acc) do
		acc
	end

	defp _remove_elem([e|t], e, acc) do
		_remove_elem(t, e, acc)
	end

	defp _remove_elem([h|t], e, acc) do
		_remove_elem(t, e, [h|acc])
	end
end
