defmodule ResDiscovery do
	use GenServer
	import ListHelper
	
	@res_discovery_server :res_discovery
	
	# --- API ---

	def start_link() do
		:net_kernel.connect(:uno@localhost)
		:gen_server.start_link({:local, @res_discovery_server}, __MODULE__, [], [])
	end

	def register_resource(res_name, pid) do
		:gen_server.call(@res_discovery_server, {:register_resource, res_name, pid})
	end

	def pids_for_resource(res_name) do
		:gen_server.call(@res_discovery_server, {:pids_for_resource, res_name})
	end

	def exchange_resources() do
		:gen_server.cast(@res_discovery_server, :exchange_resources)
	end

	def stop() do
		:gen_server.cast(@res_discovery_server, :stop)
	end

	# --- Callbacks ---

	def init(_args) do
		{:ok, {HashDict.new, HashDict.new}}
	end

	# --- :register_resource
	def handle_call({:register_resource, res_name, pid}, _from, {mine, others}) do
		new_resources = [pid |
										 Dict.get(mine, res_name, []) |> List.delete({res_name, pid})]
		new_mine = Dict.put(mine, res_name, new_resources)
		ResDiscovery.exchange_resources
		{:reply, :ok, {new_mine, others}}
	end

	#--- :pids_for_resources
	def handle_call({:pids_for_resource, res_name}, _from, {mine, others}) do
		resource_list = Dict.get(mine, res_name, []) |> Enum.concat(Dict.get(others, res_name, [])) |> remove_duplicates
		{:reply, resource_list, {mine, others}}
	end

	# --- :exchange_resources
	def handle_cast(:exchange_resources, {mine, others}) do
		IO.inspect("1 ------------------------------")

		Enum.each([node()|Node.list], &(:gen_server.cast({@res_discovery_server, &1}, {:exchange_resources, self(), mine, others})))
		{:noreply, {mine, others}}
	end

	def handle_cast({:exchange_resources, :end, its_mine, its_others}, {mine, others}) do
		IO.inspect("3 ------------------------------")

		new_others = fusion_others(others, its_mine, its_others)
		{:noreply, {mine, new_others}}
	end

	def handle_cast({:exchange_resources, from, its_mine, its_others}, {mine, others}) do
		IO.inspect("2 ------------------------------")

		new_others = fusion_others(others, its_mine, its_others)
		:gen_server.cast(from, {:exchange_resources, :end, mine, new_others})
		{:noreply, {mine, new_others}}
	end

	# --- :stop
	def handle_cast(:stop, _state) do
		{:stop, :normal, _state}
	end

	# --- Private
	defp fusion_others(my_others, its_mine, its_others) do
		new_others = Dict.merge(its_others, my_others, fn(_k, v1, v2) ->
																										 Enum.concat(v1, v2) |> remove_duplicates
														end)
		new_others = Dict.merge(new_others, its_mine, fn(_k, v1, v2) ->
																										Enum.concat(v1, v2) |> remove_duplicates
														end)
		new_others
	end

end
