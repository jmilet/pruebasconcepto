defmodule ResDiscoveryTest do
  use ExUnit.Case, async: false

	test "Register some pids under a name" do
		ResDiscovery.start_link()

		ResDiscovery.register_resource("uno", 1)
		ResDiscovery.register_resource("uno", 1)
		[1] = ResDiscovery.pids_for_resource("uno")

		ResDiscovery.register_resource("dos", 2)
		[2] = ResDiscovery.pids_for_resource("dos")

		ResDiscovery.register_resource("uno", 2)
		[1, 2] = ResDiscovery.pids_for_resource("uno") |> Enum.sort

		:rpc.call(:uno@localhost, ResDiscovery, :stop, [])
	end

	test "Exchange" do
		ResDiscovery.start_link()
		:rpc.call(:uno@localhost, ResDiscovery, :start_link, [])

		ResDiscovery.register_resource("uno", 1)
		ResDiscovery.register_resource("dos", 2)
		ResDiscovery.register_resource("dos", 22)

		:rpc.call(:uno@localhost, ResDiscovery, :register_resource, ["cien", 100])
		:rpc.call(:uno@localhost, ResDiscovery, :register_resource, ["cien", 101])

		ResDiscovery.exchange_resources()

		:timer.sleep(2000)
		
		[2, 22] = ResDiscovery.pids_for_resource("dos") |> Enum.sort
		[1] = ResDiscovery.pids_for_resource("uno")

		[100, 101] = ResDiscovery.pids_for_resource("cien") |> Enum.sort
		
		:rpc.call(:uno@localhost, ResDiscovery, :stop, [])
	end

end
