defmodule TestListHelper do
  use ExUnit.Case
	import ListHelper

	test "Test removing an element" do
		[1, 2, 4] = remove_elem([1, 2, 3, 3, 3, 4], 3)
	end

	test "Test removing duplicates" do
		[1, 2, 3, 4] = remove_duplicates([1, 2, 3, 3, 3, 4])
	end

end
