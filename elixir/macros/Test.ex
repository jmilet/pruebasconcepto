defmodule Test do

	defmacrop mia(valor) do
		quote do
	    if unquote(valor) > 5 do
	      IO.write "OK"
	    else
	  	  IO.write "MAL"
	    end
		end
	end

	def foo1, do: (x = 1; mia(x))
	def foo2, do: (x = 20; mia(x))

end
