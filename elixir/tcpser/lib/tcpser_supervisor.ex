defmodule TcpserSupervisor do
  use Supervisor

  ##############
  # Interface  #
  ##############
  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  ##############
  # Callbacks  #
  ##############
  def init(_state) do
    processes = for i <- 1..10 do
      process_id = {:p, i}
      worker(Tcpser, [process_id], id: process_id)
    end
    supervise(processes, strategy: :one_for_one)
  end
end
