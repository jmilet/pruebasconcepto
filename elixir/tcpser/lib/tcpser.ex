defmodule Tcpser do
	use GenServer

	@via_module TcpserRegister

	#############
	# Interface #
	#############
	def start_link(id) do
		GenServer.start_link __MODULE__, %{id: id}, name: via(id)
	end

	def echo(id, value) do
		GenServer.call via(id), {:echo, value}
	end

	def stop(id) do
		GenServer.cast via(id), :stop
	end

	def crash(id) do
		GenServer.call via(id), :crash
	end

	def set_timeout(id, milliseconds) do
		GenServer.call via(id), {:set_timeout, milliseconds}
	end

	def get_timeout_flag(id) do
		GenServer.call via(id), :get_timeout_flag
	end

	##############
	# Callbacks	 #
	##############
	def init(state) do
		send self(), :listen
		{:ok, state}
	end

	def handle_call({:echo, value}, _from, state) do
		{:reply, value, state}
	end

	def handle_call(:crash, _from, state) do
		raise "On purpose"
		{:reply, :crash, state}
	end

	def handle_call({:set_timeout, milliseconds}, _from, state) do
		{:reply, :ok, state, milliseconds}
	end

	def handle_call(:get_timeout_flag, _from, state) do
		{:reply, Dict.get(state, :timeout, false), state}
	end

	def handle_cast(:stop, state) do
		# Actually this call isn't necessary. See terminate below.
		TcpserRegister.unregister_name state # which is the process_id
		{:stop, :normal, state}
	end

	def handle_info(:listen, state) do
		id = Dict.get(state, :id)
		IO.inspect ">>Listening process..."
		IO.inspect id
		{:noreply, state}
	end
	
	def handle_info(:timeout, state) do
		id = Dict.get(state, :id)
		IO.inspect ">>Timeout..."
		IO.inspect id
		{:noreply, Dict.put(state, :timeout, true)}
	end

	def terminate(_reason, state) do
		IO.puts "Terminate"
	
		# It's important to note that this unregistering call, and the
		# previous one, aren't necessary the supervisor starts another
		# process up immediately, associating to the name another pid. We
		# keep this call here to have an example

		TcpserRegister.unregister_name state # which is the process_id
		:ok
	end

	############
	# private	 #
	############
	defp via(id) do
		{:via, @via_module, id}
	end

end
