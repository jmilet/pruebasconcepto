defmodule TcpserRegister do

  import Kernel, except: [send: 2]

  @process_name __MODULE__

  use GenServer

  #############
  # Interface #
  #############
  def start_link do
    GenServer.start_link TcpserRegister, [], name: @process_name
  end

  def register_name(name, pid) do
    GenServer.call @process_name, {:register_name, name, pid}
  end

  def unregister_name(name) do
    GenServer.call @process_name, {:unregister_name, name}
  end

  def whereis_name(name) do
    GenServer.call @process_name, {:whereis_name, name}
  end

  def send(name, msg) do
    case whereis_name(name) do
      :undefined ->
        Kernel.send name, msg
      pid ->
        Kernel.send pid, msg
    end
  end

  def registered do
    GenServer.call @process_name, :registered
  end

  ##############
  # Callbacks  #
  ##############
  def init(_) do
    {:ok, HashDict.new}
  end

  def handle_call({:register_name, name, pid}, _from, state) do
    Process.monitor(pid)
    {:reply, :yes, Dict.put(state, name, pid)}
  end

  def handle_call({:unregister_name, name}, _from, state) do
		IO.puts "Unregister"
    {:reply, :ok, Dict.delete(state, name)}
  end

  def handle_call({:whereis_name, name}, _from, state) do
    {:reply, Dict.get(state, name, :undefined), state}
  end

  def handle_call(:registered, _from, state) do
    {:reply, Enum.sort(Dict.keys(state)), state}
  end

  def handle_info({:DOWN, _ref, _type, pid, _reason}, state) do
    new_state = remove_pid(state, pid)
    {:noreply, new_state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

	###########
  # Private #
  ###########

	defp remove_pid(processes, pid) do
		(for {k, v} <- Dict.to_list(processes), v != pid, do: {k, v}) |> Enum.into HashDict.new
	end
end
