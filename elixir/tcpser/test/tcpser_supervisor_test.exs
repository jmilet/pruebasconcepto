defmodule TcpserSupervisorTest do
  use ExUnit.Case

  setup do
    # Starts the register process.
    TcpserRegister.start_link

    # Starts the supervisor.
    TcpserSupervisor.start_link

    {:ok, %{}}
  end
  
  @tag :tcpser_supervisor_test1
  test "Starting the supervisor and testing the process is alive" do
    process_id = {:p, 1}

    # Testing the process is out there.
    assert "hola" == Tcpser.echo process_id, "hola"
  end

  @tag :tcpser_supervisor_test2
  test "Starting the supervisor, and crashing the process" do
    process_id = {:p, 1}

    # Testing the process is out there.
    assert "hola" == Tcpser.echo process_id, "hola"

    # Save the first process's pid.
    first_pid = TcpserRegister.whereis_name process_id

    # Crashing it.
    catch_exit Tcpser.crash process_id

    :timer.sleep(1000)

    # The process must be there.
    assert "hola" == Tcpser.echo process_id, "hola"

    # Save the second process's pid.
    second_pid = TcpserRegister.whereis_name process_id

    # The pids must be different.
    assert first_pid != second_pid
  end

  @tag :tcpser_supervisor_test3
  test "Starting the supervisor, and crashing all the process" do
    for i <- 1..10 do
      process_id = {:p, i}
      
      # Testing the process is out there.
      assert "hola" == Tcpser.echo process_id, "hola"

      # Crashing it.
      catch_exit Tcpser.crash process_id

      :timer.sleep(2000)

      # Testing the process is out there.
      assert "hola" == Tcpser.echo process_id, "hola"
    end
  end
end
