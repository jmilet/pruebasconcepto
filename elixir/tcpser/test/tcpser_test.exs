defmodule TcpserTest do
  use ExUnit.Case

  @tag :tcpser1
  test "Tcpser is running" do
    # Starts our own via register.
    TcpserRegister.start_link

    # Starts a process with 10 id.
    Tcpser.start_link 10

    # Echoes and string. Check it comes back.
    assert "bien" == Tcpser.echo 10, "bien"

    # Stops the server and check there's no answer.
    Tcpser.stop 10
    catch_exit Tcpser.echo 10, "bien"
  end

  @tag :tcpser2
  test "Tcpser times out as requested" do
    # Starts our own via register.
    TcpserRegister.start_link

    # Starts a process with 10 id.
    Tcpser.start_link 10

    # Sets a new timeout.
    Tcpser.set_timeout 10, 100
    
    # Wait.
    :timer.sleep(2000)

    # The timeout flag has been set.
    assert true == Tcpser.get_timeout_flag 10
  end
end
