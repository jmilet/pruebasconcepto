defmodule TcpserRegisterTest do
  use ExUnit.Case, async: false

  setup do
    # Star our own via register.
    TcpserRegister.start_link

    {:ok, HashDict.new}
  end

  @tag :tcpregister1
  test "Registers a process and checks it's registered" do
    # Check it's registered.
    process_id = {:p, 1}
    {:ok, pid} = Tcpser.start_link process_id
    assert pid == TcpserRegister.whereis_name process_id
  end

  @tag :tcpregister2
  test "Registers a process and echo it" do
    # Echo it.
    process_id = {:p, 1}
    Tcpser.start_link process_id
    assert "bien" == Tcpser.echo process_id, "bien"
  end

  @tag :tcpregister3
  test "Registers a process, stop it and checks it's not registered" do
    # Start a first process.
    first_process_ID = {:p, 1}
    {:ok, first_process_pid} = Tcpser.start_link first_process_ID

    # Starts and stops a second one.
    second_process_ID = {:p, 2}
    Tcpser.start_link second_process_ID
    Tcpser.stop second_process_ID

    # Waits for the second process.
    :timer.sleep(1000)

    # Checks it's no longer running.
    catch_exit Tcpser.echo second_process_ID, "bien"
    
    # It must not be registered.
    assert :undefined == TcpserRegister.whereis_name second_process_ID

    # The first process it's still registered.
    assert first_process_pid == TcpserRegister.whereis_name first_process_ID
  end

  @tag :tcpregister4
  test "Spawn many processes" do
    # Number of processes to spawn.
    number_of_processes = 4

    # First spawn.
    initial_process_list = Enum.map(1..number_of_processes, fn(x) ->
                                                              Tcpser.start_link {:p, x}
                                    end)
    # Second spawn.
    second_process_list = Enum.map(1..number_of_processes, fn(x) ->
                                                             {:ok, TcpserRegister.whereis_name {:p, x}}
                                   end)

    # Both must be identical.
    assert initial_process_list == second_process_list

    IO.inspect second_process_list

    # Stops a process and wait for it.
    process_id = {:p, 2}
    Tcpser.stop process_id
    :timer.sleep(1000)

    # It must not registered.
    assert :undefined == TcpserRegister.whereis_name process_id
  end

  @tag :tcpregister5
  test "List of registered processes" do
    # Starts some processes.
    for x <- 1..5, do: Tcpser.start_link {:p, x}

    # All of them must be registed.
    assert (for x <- 1..5, do: {:p, x}) == TcpserRegister.registered
  end

	@tag :tcpregister6
	test "Crashes the process and check it's deregistered" do
		# We have to trap exits in order not to be dragged by
		# the crashing process.
		Process.flag(:trap_exit, true)

		process_id = {:p, 1}

		# Starts the process.
		{:ok, pid} = Tcpser.start_link process_id

		# Catches the exception.
		catch_exit Tcpser.crash process_id

		assert :undefined == TcpserRegister.whereis_name process_id
	end

	@tag :tcpregister7
	test "Stops the process and check it's deregistered" do
		process_id = {:p, 1}

		# Starts the process.
		{:ok, pid} = Tcpser.start_link process_id

		# Normally stops the process.
		Tcpser.stop process_id

		assert :undefined != TcpserRegister.whereis_name process_id
	end
end
