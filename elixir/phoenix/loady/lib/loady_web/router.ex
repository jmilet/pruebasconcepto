defmodule LoadyWeb.Router do
  use LoadyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LoadyWeb do
    pipe_through :browser # Use the default browser stack

    get "/", MainController, :index
    get "/ping", PingController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", LoadyWeb do
    pipe_through :api

    get "/hello/:name", HelloControler, :index
  end
end
