defmodule LoadyWeb.CpuChannel do
    use Phoenix.Channel
  
    def join("load:cpu", _message, socket) do
      {:ok, socket}
    end

    def join(_other, _params, _socket) do
      {:error, %{reason: "unauthorized"}}
    end

    def handle_in("start", _value, socket) do
        spawn_link(fn -> loop(socket) end)
        {:noreply, socket}
    end

    defp loop(socket) do
      speed = 1000

      data = %{
        "cpu": :random.uniform(100),
        "speed": speed
      }

      IO.inspect data
      push(socket, "cpu", data)
      :timer.sleep(speed)
      loop(socket)
    end
  end