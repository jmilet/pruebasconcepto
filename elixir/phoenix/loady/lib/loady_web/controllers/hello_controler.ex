defmodule LoadyWeb.HelloControler do
    use LoadyWeb, :controller

    def index(conn, params) do
        render conn, "index.json", params
    end
end