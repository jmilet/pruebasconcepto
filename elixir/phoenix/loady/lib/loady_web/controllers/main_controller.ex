defmodule LoadyWeb.MainController do
  use LoadyWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
