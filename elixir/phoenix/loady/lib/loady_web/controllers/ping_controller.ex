defmodule LoadyWeb.PingController do
    use LoadyWeb, :controller

    def index(conn, _params) do
        render conn, "index.html", test_parameter: "ok"
    end    
end