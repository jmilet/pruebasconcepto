
defmodule DocExamples do
	@moduledoc """
  Documentación del módulo.
  """

	@svn 0.1

	@doc """
  Returns a * 2.
  """
	@spec foo(number()) :: number()
	def foo(a) do
		a * 2
	end

	@doc """
  Retorns a * b where b defaults to 10.
  """
	@spec foa(number()) :: number()
	def foa(a, b \\ 10) do
		a * b
	end

end
