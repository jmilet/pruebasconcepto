defmodule Repeat do

	def norepeated(list) do
		Enum.reverse _norepeated(list, [])
	end

	defp _norepeated([], result) do
		result
	end

	defp _norepeated([a, a|resto], result) do
		_norepeated([a|resto], result)
	end

	defp _norepeated([a|resto], result) do
		_norepeated(resto, [a|result])
	end

end
