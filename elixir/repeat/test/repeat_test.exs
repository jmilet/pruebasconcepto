defmodule RepeatTest do
  use ExUnit.Case

  test "Basic" do
    assert Repeat.norepeated([1, 2, 3]) == [1, 2, 3]
  end

  test "One repetition" do
    assert Repeat.norepeated([1, 2, 2, 3]) == [1, 2, 3]
  end

  test "Two repetition" do
    assert Repeat.norepeated([1, 2, 2, 3, 3, 3, 4]) == [1, 2, 3, 4]
  end

  test "Three repetition" do
    assert Repeat.norepeated([1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5]) == [1, 2, 3, 4, 5]
  end

end
