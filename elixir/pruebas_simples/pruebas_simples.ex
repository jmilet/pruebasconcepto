defmodule Times do

	def double (n) do
		n * 2
	end
end

defmodule Guard do
	def what_is(x) when is_number(x) do IO.puts "#{x} is a number"
	end
	def what_is(x) when is_list(x) do IO.puts "#{inspect(x)} is a list"
	end
	def what_is(x) when is_atom(x) do IO.puts "#{x} is an atom"
	end
end


defmodule Cadena do

	def uno param do
		IO.puts param
	end

	def dos param, n do
		IO.puts "#{param}#{n + 2}"
	end

end

defmodule EjemploAtributos do

	@valor "Un valor"

	def mostrar_valor, do:	IO.puts @valor

end

IO.puts Times.double 10
Guard.what_is [1,2]

(1..10) |> Enum.map(&(&1 + 2)) |> Enum.map(&(IO.puts &1))

EjemploAtributos.mostrar_valor

:"Elixir.IO".puts "Llamando desde atom"

:io.format("Llamando desde Elixir a Erlang ~3.1f~n", [5.678])