defmodule Phoenixtest.Saver do
    use GenServer

    def start_link(topic) do
        GenServer.start_link __MODULE__, topic, name: {:global, topic}
    end

    def init(topic) do
        IO.inspect "Starting topic #{topic}"
        {:ok, file} = File.open topic <> ".txt", [:write]
        {:ok, file}
    end

    def write(topic, value) do
        GenServer.cast {:global, topic}, {:write, value}
    end

    def die(topic) do
        GenServer.cast {:global, topic}, :die
    end

    def handle_cast({:write, value}, file) do
        IO.binwrite file, value <> "\n"
        {:noreply, file}
    end

    def handle_cast(:die, file) do
        {:stop, :die, file}
    end
end