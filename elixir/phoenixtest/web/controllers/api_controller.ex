defmodule Phoenixtest.ApiController do
  use Phoenixtest.Web, :controller

  def index(conn, %{"topic" => topic, "value" => "die"}) do
    Phoenixtest.Saver.die topic
    text conn, "ok"
  end

  def index(conn, %{"topic" => topic, "value" => value}) do
    Phoenixtest.Saver.write topic, value
    text conn, "ok"
  end
end
