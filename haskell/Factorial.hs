module Factorial where

fac dd
  | n == 0   = 2
  | n > 0    = fac (n - 1) * n
