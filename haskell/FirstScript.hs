

module FirstScript where

size :: Integer
size = 12 + 13

square :: Integer -> Integer
square n = n * n

double :: Integer -> Integer
double n = n * 2

example :: Integer
example = double (size - square (2 + 2))

func1 :: Integer -> Integer
func1 n = square(double n)

func2 :: Integer -> Integer
func2 n = double(square n)


myNot :: Bool -> Bool
myNot True = False
myNot False = True

exOr :: Bool -> Bool -> Bool
exOr x y = (x || y) && not (x && y)

exOr1 :: Bool -> Bool -> Bool
exOr1 True x = not x
exOr1 False x = x



prop_myNot :: Bool -> Bool

prop_myNot x =
  not x == myNot x

prop_exOrs :: Bool -> Bool -> Bool

prop_exOrs x y =
  exOr x y == exOr1 x y



threeEqual :: Integer -> Integer -> Integer -> Bool
threeEqual m n p = (m == n) && (n == p)

myMax :: Integer -> Integer -> Integer
myMax x y
  | x >= y = x
  | otherwise = y


maxThree :: Integer -> Integer -> Integer -> Integer
maxThree x y z
  | x >= y && x >= z     = x
  | y >= z               = y
  | otherwise            = z

max' :: Integer -> Integer -> Integer
max' x y
  = if x >= y then x else y

prop_compareMax :: Integer -> Integer -> Bool
prop_compareMax x y =
  max x y == max' x y


-- Crates a People's instance with the Persona constructor.
type Name = String
type Grade = Integer
data People = Persona Name Grade deriving (Eq, Show, Read)
--(read (show (Persona "juan" 30)))::People
