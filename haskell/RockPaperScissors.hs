module RockPaperScissors where

data Move = Rock | Paper | Scissors
  deriving (Show, Eq)

beat :: Move -> Move

beat Rock     = Paper
beat Paper    = Scissors
beat Scissors = Rock

lose :: Move -> Move

lose Rock  = Scissors
lose Paper = Rock
lose _     = Paper
