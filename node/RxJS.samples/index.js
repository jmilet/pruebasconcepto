Observable = require('rxjs').Observable;
from = require('rxjs').from;
range = require('rxjs').range;
of = require('rxjs').of;
interval = require('rxjs').interval;
map = require('rxjs/operators').map;
take = require('rxjs/operators').take;
catchError = require('rxjs/operators').catchError;
retry = require('rxjs/operators').retry;
filter = require('rxjs/operators').filter;
distinctUntilChanged = require('rxjs/operators').distinctUntilChanged;
scan = require('rxjs/operators').scan;
window = require('rxjs/operators').window;
mergeAll = require('rxjs/operators').mergeAll;
bufferTime = require('rxjs/operators').bufferTime;
bufferCount = require('rxjs/operators').bufferCount;



function test_001() {
  const observervable = Observable.create(observer => {
    observer.next('Simon');
    observer.next('Jen');
    observer.next('Sergi');
    observer.complete();
  });

  const observer = observervable.subscribe(
    x => console.log('Next: ' + x),
    err => console.log('Error: ' + err),
    () => console.log('Completed')
  );

  const observer2 = observervable.subscribe(
    x => console.log('Next: ' + x),
    err => console.log('Error: ' + err),
    () => console.log('Completed')
  );
}

function test_002() {
  const avg = interval(1000)
    .pipe(
      scan((prev, cur) => {
        return {
          sum: prev.sum + cur,
          count: prev.count + 1
        };
      }, {sum: 0, count: 0}),
      map(o => {
        return o.sum / o. count;
      })
    );

  const subscription = avg.subscribe(x => console.log('uno: ' + x));

  const subscription2 = avg
    .pipe(
      filter(x => x % 2 === 0),
      take(3))
    .subscribe(x => console.log('dos:' + x),
               null,
               () => console.log("dos finished...!!!!!!!!!!!!!!!!!!!!!!"));
}

function test_003() {
  const ticker = interval(2000);

  const single = ticker.subscribe(x => console.log('single:' + x));
  const double = ticker.subscribe(x => console.log('doble: ' + (x * 2)));

  setTimeout(() => {
    single.unsubscribe();
    console.log('single: unsubscribed... !!!!!!');

    ticker.subscribe(x => console.log('triple: ' + x));

    }, 5000);
}

function test_004() {
  from(['{a: 100}', '[1, 2, 3]'])
    .pipe(
      map(str => JSON.parse(str)),
      catchError(err => of('Failed reason is: ' + err))
    ).subscribe(json => console.log(json),
                 err => console.log(err.message))
}

function test_005() {
  function get() {
    let times = 0;
    
    return range(1, 4)
      .pipe(map(x => {
              times++;

              if (times <= 3) {
                console.log('get fails time: ' + times);
                throw 'get error';
              }
              else {
                console.log('get ok time: ' + times);
                return x;
              }
            })
      );
  }

  get().pipe(retry(5)).subscribe(x => console.log(x));
}

function test_006() {
  from([1, 1, 1, 1, 2, 3, 3, 3, 3, 3, 4, 5])
    .pipe(distinctUntilChanged(),
          filter(x => x > 2)
    ).subscribe(x => console.log(x));
}

function test_007() {
  interval(100).pipe(
    filter(x => x % 5 === 0),
    bufferTime(3000)
  ).subscribe(x => console.log(x));
}

function test_008() {
  const source$ = interval(250);

  source$.pipe(
    filter(x => x % 2 === 0),
    map(x => x + x),
    scan((acc, x) => acc + x, 0),
    bufferCount(3)
  )
  .subscribe(x => console.log(x))
}
// test_001();
// test_002();
// test_003();
// test_004();
// test_005();
// test_006();
// test_007();
test_008();
