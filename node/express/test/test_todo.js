const vows = require('vows')
const assert = require('assert');
const Todo = require('../todo')

vows.describe('Test one').addBatch({
    'when we try to add an element': {
        topic: () => {
            const todo = new Todo();
            todo.add('An element');
            return todo;
        },
        'it should add it': (err, todo) => {
            assert.equal(todo.getLength(), 1);
        }
    }
}).export(module);

