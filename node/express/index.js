'use strict'

// curl --data '{"a": 100}' -H "Content-Type: application/json" http://localhost:3000/test

const express = require('express')
const bodyParser = require('body-parser')

function connectToMongo(logic) {
    // Mongo
    var mongoClient = require('mongodb').MongoClient;
    var url = `mongodb://${process.env.MONGO_PORT_27017_TCP_ADDR}:${process.env.MONGO_PORT_27017_TCP_PORT}/test`;
    console.log(url);

    mongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Database created!");
        logic(db);
    });
}


function save(db, body, res) {
    var dbo = db.db("mydb");
    dbo.collection("companies").insertOne(body, function(err, result) {
      if (err) throw err;
      console.log("1 document inserted");
      res.set('Content-Type', 'text/plain');
      res.send('')
  });
}

function list(db, res) {
    var dbo = db.db("mydb");
    dbo.collection("companies").find().toArray(function(err, result) {
        if (err) throw err;
        res.set('Content-Type', 'application/json');
        res.json(result);
    });
}

function runApp(db) {
    // Express
    const app = express();
    
    app.use(bodyParser.json());
    
    app.post('/insert', function(req, res) {
        save(db, req.body, res);
    });
   
    app.get('/list', function(req, res) {
        list(db, res);
    });

    app.listen(3000, '0.0.0.0', function (err) {
        if (err) {
            throw err;
        }
        
        console.log('Server started on port 3000');
    });
}

function main() {
    var db = connectToMongo(function(db) {
        //runConsumer(db);
        runApp(db);
    });
}

main();