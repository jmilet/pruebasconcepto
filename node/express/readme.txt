npm install express --save
npm install mongodb --save
npm install vows


curl --data '{"a": 100}' -H "Content-Type: application/json" http://localhost:3000/insert


docker run --rm --name some-mongo mongo
docker run --rm -it --name some-app --link some-mongo:mongo -p 3000:3000 -v `pwd`:/app node sh -c 'node /app/index.js'
docker run --rm -it --link some-mongo:mongo --rm mongo sh -c 'exec mongo "$MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT/test"'


vows test/*
