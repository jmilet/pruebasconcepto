let config = {
    connString: process.env.DEMO_CONN_STRING,
    env: process.env.DEMO_ENV,
};

module.exports = Object.assign(config,
    require('../module1/config'),
    require('../module2/config'),
);