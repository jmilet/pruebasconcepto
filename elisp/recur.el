
(defun foo(n l)
  (if (= n 0)
      l
    (foo (- n 1) (cons n l))))


(reverse (foo 5 '()))

