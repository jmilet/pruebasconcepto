

(defmacro unless(expr action)
  `(if (not ,expr)
      ,action))


(unless (not nil) :ok)
(unless nil :ok)

(defmacro transaction(expr)
  `(cons :ini (cons ,expr (cons :fin ()))))

(transaction (* 1 2))


