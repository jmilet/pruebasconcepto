# -*- coding: latin-1 -*-

import os
import sys
import hashlib

def siguiente_elemento(it):
    try:
        elem = it.next()
    except StopIteration:
        return None

    return  elem

def recorrer(directorio):
    salida = {}
    for root, subfolders, files in os.walk(directorio):
        for f in files:
            #print root + "/" + f
            with open(root + "/" + f, "rt") as fichero:
                salida[f] = (f, hashlib.md5(fichero.read()).hexdigest())
    
    return salida

def sort_merge(dir_a, dir_b):
    it_a, it_b = iter(sorted(dir_a.values())), iter(sorted(dir_b.values()))

    a = siguiente_elemento(it_a)
    b = siguiente_elemento(it_b)

    while a and b:
        if a == b:
            print "===", a
            a = siguiente_elemento(it_a)
            b = siguiente_elemento(it_b)
        elif a > b:
            print ">>>", b
            b = siguiente_elemento(it_b)
        else:
            print "<<<", a
            a = siguiente_elemento(it_a)
        
    while a:
        print "<<<", a
        a = siguiente_elemento(it_a)

    while b:
        print ">>>", b
        b = siguiente_elemento(it_b)

def main(argv):
    if len(sys.argv) != 3:
        print "error de parámetros"
        exit(1)

    dir_a = recorrer(argv[1])
    dir_b = recorrer(argv[2])

    sort_merge(dir_a, dir_b)

if __name__ == '__main__':
    main(sys.argv)
