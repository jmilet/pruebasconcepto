import SocketServer
import json
import DataBase
import time
import threading



database = DataBase.DataBase()

def obtener_plugin_comando(comando):
    py_file = "plugin_%s" % comando
    modulo = __import__(py_file)
    return modulo.plugin()

class MyTCPHandler(SocketServer.StreamRequestHandler):

    def handle(self):
        try:
            fin = False
            while not fin:
                entrada = self.rfile.readline().strip()
                print "+++|%s|" % entrada
	        if len(entrada) <= 0:
	            continue
                entrada = json.loads(entrada)
                comando = obtener_plugin_comando(entrada["command"])
                fin, respuesta = comando.run(entrada, database)
                print "---|%s|" % respuesta
                self.wfile.write(respuesta + "\n")
                self.wfile.flush()
        except:
            pass

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9999

    # Create the server, binding to localhost on port 9999
    server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)

    ###server.serve_forever()
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = False
    server_thread.start()
