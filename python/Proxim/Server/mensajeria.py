
def create_return_dict(code, message):
	return dict(code=code, message=message)

def create_OK_return_dict(entrada):
	return dict(command=entrada['command'], command_seq=entrada['command_seq'], timestamp=entrada['timestamp'], code=0, message="")

