import json
import DataBase
import mensajeria

class plugin(object):

	def run(self, entrada, db):

		user_id = entrada['user_id']
		longitud = entrada['longitud']
		latitud = entrada['latitud']

		user = DataBase.User(user_id=user_id, longitud=longitud, latitud=latitud)

		db.add_user(user)
		
		#print "+++ Conectado usuario %s %s, %s - %s" % (user_id, longitud, latitud, entrada)

		retorno = mensajeria.create_OK_return_dict(entrada)
		retorno['user_list'] = db.users().values()

		return False, json.dumps(retorno, cls=DataBase.DataBaseJSONDecoder)
