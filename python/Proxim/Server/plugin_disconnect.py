import json
import DataBase
import mensajeria

class plugin(object):

	def run(self, entrada, db):

		user_id = entrada['user_id']

		db.del_user(user_id)
		
		#print "--- Conectado usuario %s - %s" % (user_id, entrada)

		retorno = mensajeria.create_OK_return_dict(entrada)

		return True, json.dumps(retorno, cls=DataBase.DataBaseJSONDecoder)
