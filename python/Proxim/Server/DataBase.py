import json

class DataBaseJSONDecoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, User):
			return obj.__dict__

		return json.JSONEncoder.default(self, obj)

class User(object):

	def __init__(self, user_id, longitud, latitud):
		self.user_id = user_id
		self.longitud = longitud
		self.latitud = latitud

class DataBase(object):

    def __init__(self):
        self._users = {}

    def add_user(self, user):
    	self._users[user.user_id] = user

    def del_user(self, user):
    	self._users.pop(user)

    def users(self):
    	return self._users
