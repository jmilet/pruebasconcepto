package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func workerTopic(topic string, values <-chan string, errors chan<- string) {
	log.Println(fmt.Sprintf("Running topic %s", topic))

	topicFileName := topic + ".out"
	file, err := os.Create(topicFileName)
	if err != nil {
		log.Fatal("Unable to open file")
	}

	for value := range values {
		//time.Sleep(3 * time.Second)
		file.WriteString(fmt.Sprintf("%s => %s\n", topic, value))
		if value == "error" {
			errors <- topic + "=>" + value
		}
	}
}

func workerError(errors chan string) {
	log.Println("Running errors...")

	for error := range errors {
		log.Printf("Error topic %s", error)
	}
}

func createTopicWorker(topicName string, errors chan<- string) chan<- string {
	topicChannel := make(chan string, 10000)
	go workerTopic(topicName, topicChannel, errors)
	return topicChannel
}

func createErrorsWorker() chan<- string {
	errors := make(chan string)
	go workerError(errors)
	return errors
}

func createHandel(topicsRoutines map[string]chan<- string, errors chan<- string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatal("Error in reading the body")
		}
		values := strings.Split(string(body), ":")
		topic := values[0]
		value := values[1]

		channel := topicsRoutines[topic]
		if channel != nil {
			topicsRoutines[topic] <- value
		} else {
			errors <- fmt.Sprintf("Topic %s not found", topic)
		}
	}
}

func createTopicWorkers(topicNames []string, errors chan<- string) map[string]chan<- string {
	topicWorkers := make(map[string]chan<- string)
	for _, topicName := range topicNames {
		topicWorkers[topicName] = createTopicWorker(topicName, errors)
	}
	return topicWorkers
}

func main() {
	topicNames := strings.Split(os.Args[1], ",")

	errors := createErrorsWorker()
	topicWorkers := createTopicWorkers(topicNames, errors)

	http.HandleFunc("/foo", createHandel(topicWorkers, errors))

	log.Println("Starting server...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
