package jmilog

// Jmilog interface.
type Jmilog interface {
	Info(text string)
}
