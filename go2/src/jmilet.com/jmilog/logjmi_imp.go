package jmilog

import (
	"fmt"
)

// BasicLog is a log implementation
type BasicLog struct {
}

// Info implements the info function
func (log BasicLog) Info(text string) {
	fmt.Println(">>>" + text)
}
