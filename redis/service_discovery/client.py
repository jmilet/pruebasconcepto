import time
import random
import json
import sys
import redis
import sd

def main(argv):
    conn = redis.Redis()

    types = ['mongo', 'mysql', 'webserver1']
   
    while True:
        for type in types:
            for ip in random.sample(range(250), 150):
                sd.add_client(conn, type, json.dumps({'type': type, 'ip': ip}))
 
        for k, v in sd.info(conn).items():
            print("%20s => %d" % (k.decode("utf-8"), len(v)))
        
        print("")

        time.sleep(1)

main(sys.argv)
