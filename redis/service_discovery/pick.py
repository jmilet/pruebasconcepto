import sys
import time
import random
import redis
import sd

def main(argv):
    random.seed(time.time())

    conn = redis.Redis()

    types = ['mongo', 'mysql', 'webserver1']

    while True:
        for type in types:
            server = sd.pick_one(conn, type)
            if server:
                print(server.decode("utf-8"))
        
        print("")
        time.sleep(1)
 
main(sys.argv)