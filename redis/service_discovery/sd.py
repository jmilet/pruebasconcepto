import redis
import time
import sys
import random
import json
import argparse

ROOT_KEY = "services:"

def purge(conn, offset=3):
    for key in conn.keys(ROOT_KEY + "*"):
        pipe = conn.pipeline()
        now = time.time()
        pipe.zremrangebyscore(key, 0, now - offset)
        pipe.execute()

def info(conn):
    ret = {}

    for key in conn.keys(ROOT_KEY + "*"):
        keys = conn.zrange(key, 0, -1)
        ret[key] = keys

    return ret

def add_client(conn, type, value):
    conn.zadd(ROOT_KEY + type, value, time.time())

def pick_one(conn, type):
    elements = conn.zrange(ROOT_KEY + type, 0, -1)

    if not elements:
        return None
        
    return elements[random.randint(0, len(elements) - 1)]
