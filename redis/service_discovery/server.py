import time
import sys
import redis
import sd

def main(argv):
    conn = redis.Redis()

    while True:
        sd.purge(conn)
        time.sleep(1)

main(sys.argv)