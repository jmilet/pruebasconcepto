import java.io.IOException;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class ConsumerTest {

    private static final String EXCHANGE_NAME = "hello-exchange";

    public static void main(String[] argv)
	throws java.io.IOException,
	       java.lang.InterruptedException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
	factory.setUsername("guest");
	factory.setPassword("guest");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "direct", true /* durable */ );
        String queueName = "hello-queue";
	channel.queueDeclare(queueName, false /* durable */ , false /* exclusive */ , true /* autodelete */, null /* arguments */);
        channel.queueBind(queueName, EXCHANGE_NAME, "hola");
	channel.basicQos(/* prefetch */ 1);

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, false /* autoAck */, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
	    Thread.sleep(1000);
	    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false /* multiple */ );

            System.out.println(" [x] Received '" + message + "'");
        }
    }
}
