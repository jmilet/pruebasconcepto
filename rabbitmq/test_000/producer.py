import pika, sys
import time, random


credentials = pika.PlainCredentials("guest", "guest")
conn_params = pika.ConnectionParameters("localhost",
                                        credentials = credentials)


conn_broker = pika.BlockingConnection(conn_params)
channel = conn_broker.channel()

channel.exchange_declare(exchange="hello-exchange",
                         type="direct",
                         passive=False,
                         durable=True,
                         auto_delete=False)

#msg = sys.argv[1]
limit = int(sys.argv[1])
msg_props = pika.BasicProperties()
msg_props.content_type = "text/plain"

for msg in range(limit):
   print msg
   channel.basic_publish(body=str(msg) + " hola" * 10,
                         exchange="hello-exchange",
                         properties=msg_props,
                         routing_key="hola")
   #time.sleep(random.uniform(0, .2))
