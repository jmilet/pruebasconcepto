-module(socket_gen).
-behaviour(gen_server).
-export([start_link/1, init/1, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

-record(state, {socket, accept_socket}).

%% Interface.
start_link(Socket)->
    gen_server:start_link(?MODULE, Socket, []).

%% Funciones servidor.
init(Socket)->
    io:format("En ini"),
    gen_server:cast(self(), accept),
    {ok, #state{socket=Socket}}.

handle_call(_E, From, State)->
    {reply, From, State}.

handle_cast(accept, State=#state{socket=Socket})->
    AcceptSocket = gen_tcp:accept(Socket),
    %%inet:setopts(AcceptSocket, [{active, true}]),
    socket_sup:start_socket(),
    {noreply, State#state{accept_socket=AcceptSocket}}.

handle_info({tcp, Socket, Msg}, State)->
    gen_tcp:send(Socket, Msg ++ "\n"),
    inet:setopts(Socket, [{active, once}]),
    {noreply, State};
handle_info({tcp_closed, AcceptSocket}, _) ->
    gen_tcp:close(AcceptSocket),
    %%AcceptSocket = gen_tcp:accept(Socket),
    %%{noreply, State#state{accept_socket=AcceptSocket}}.
    {stop, normal, fin}.

terminate(normal, _State)->
    io:format("Terminando..."),
    ok.

code_change(_OldState, State, _Extra)->
    {ok, State}.
