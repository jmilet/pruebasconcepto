-module(test_distributed).
-include_lib("eunit/include/eunit.hrl").

all_test_() ->
    {setup,
     fun start/0,
     fun stop/1,
     fun (Config) ->
	     {with, Config, [
			     fun register/1,
			     fun group/1
			    ]}
     end}.

start() ->
    ?debugMsg("Testing start......."),
    application:start(sasl),
    error_logger:tty(false),
    application:start(distributed).

stop(_Config) ->
    ?debugMsg("Testing stop......."),
    application:stop(distributed),
    application:stop(sasl).

register(_Config) ->
    Process = spawn(fun() -> receive X -> ok end end),
    register:register_name("p1", Process),
    Yo = register:whereis_name("p1"),
    undefined = register:whereis_name("p2"),
    register:unregister_name("p1"),
    [] = register:registered().

group(_Config) ->
    super_servidor:start_child("juan"),
    3 = servidor:sum("juan", 1, 2),
    exit(register:whereis_name("juan"), kill),
    timer:sleep(1000),
    4 = servidor:sum("juan", 1, 3).
