-module(app_distributed).

-behaviour(application).

-export([start/2, stop/1, start/0]).

% This functions is required by -s erl's param.
start() ->
    application:start(distributed).

start(_StartType, _StartArgs) ->
    case super_top:start_link() of
	{ok, Pid} ->
	    {ok, Pid};
	Error ->
	    Error
    end.

stop(_State) ->
    ok.
