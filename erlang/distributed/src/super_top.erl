-module(super_top).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
    RestartStrategy = one_for_all,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = worker,

    RegisterSupervisor = {super_register, {super_register, start_link, []},
			  Restart, Shutdown, Type, [super_register]},

    ServidorSupervisor = {super_servidor, {super_servidor, start_link, []},
			  Restart, Shutdown, Type, [super_servidor]},

    {ok, {SupFlags, [RegisterSupervisor, ServidorSupervisor]}}.
