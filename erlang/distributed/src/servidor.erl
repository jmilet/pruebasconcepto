-module(servidor).

-behaviour(gen_server).

-export([start_link/1, sum/3]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

start_link(Name) ->
    gen_server:start_link(?MODULE, [Name], []).

sum(Server, A, B) ->
    gen_server:call({via, register, Server}, {sum, A, B}).

init([Name]) ->
    io:format("Starting 'servidor' with pid ~p~n", [self()]),
    register:register_name(Name, self()),
    {ok, #state{}}.

handle_call({sum, A, B}, _From, State) ->
    {reply, A + B, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
