-module(register).

-behaviour(gen_server).

-export([start_link/0, register_name/2, unregister_name/1, registered/0, whereis_name/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {names=dict:new(), pids=dict:new()}).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

register_name(Name, Pid) ->
    gen_server:call(?SERVER, {register_name, Name, Pid}).

unregister_name(Name) ->
    gen_server:call(?SERVER, {unregister_name, Name}).

registered() ->
    gen_server:call(?SERVER, registered).

whereis_name(Name) ->
    gen_server:call(?SERVER, {whereis_name, Name}).

init([]) ->
    io:format("Starting 'register' with pid ~p~n", [self()]),
    {ok, #state{}}.

handle_call({register_name, Name, Pid}, _From, State) ->
    Names = State#state.names,
    Pids = State#state.pids,
    case dict:is_key(Name, Names) of
	true ->
	    {reply, badarg, State};
	false ->
	    monitor(process, Pid),
	    {reply, yes, State#state{names=dict:store(Name, Pid, Names), pids=dict:store(Pid, Name, Pids)}}
    end;
handle_call({unregister_name, Name}, _From, State) ->
    Names = State#state.names,
    Pids = State#state.pids,
    case dict:find(Name, Names) of
	{ok, Pid} ->
	    {reply, yes, State#state{names=dict:erase(Name, Names), pids=dict:erase(Pid, Pids)}};
	error ->
	    {reply, yes, State}
    end;
handle_call(registered, _From, State) ->
    Names = State#state.names,
    {reply, dict:fetch_keys(Names), State};
handle_call({whereis_name, Name}, _From, State) ->
    Names = State#state.names,
    Pid = case dict:find(Name, Names) of
	      {ok, Value} ->
		  Value;
	      error ->
		  undefined
	  end,
    {reply, Pid, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({'DOWN', _Ref, process, Pid, _Reason}, State) ->
    io:format("Down process with pid ~p~n", [Pid]),
    Names = State#state.names,
    Pids = State#state.pids,
    Name = dict:fetch(Pid, Pids),
    {noreply, State#state{names=dict:erase(Name, Names), pids=dict:erase(Pid, Pids)}};
handle_info(_Info, State) ->
    io:format("~p~n", [_Info]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

    
