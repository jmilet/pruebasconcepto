-module(super_servidor).

-behaviour(supervisor).

-export([start_link/0, start_child/1]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    io:format("Starting 'super_servidor' with pid ~p~n", [self()]),
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

start_child(Name) ->
    supervisor:start_child(?SERVER, [Name]).

init([]) ->
    io:format("Starting 'super' with pid ~p~n", [self()]),

    RestartStrategy = simple_one_for_one,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = worker,

    AChild = {'s1', {servidor, start_link, []},
	      Restart, Shutdown, Type, [servidor]},

    {ok, {SupFlags, [AChild]}}.
