-module(gen_event_test).
-behaviour(gen_event).
-export([init/1, handle_event/2, terminate/2]).


init(Args) ->
    io:format("~p~n", [Args]),
    {ok, Args}.

handle_event(Event, State) ->
    io:format("~p-~p~n", [Event, State]), 
    {ok, State + 1}.

terminate(Event, State) ->
    io:format("~p-~p~n", [Event, State]).    
