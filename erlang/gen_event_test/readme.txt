$ /usr/local/bin/erl
Eshell V5.10.3  (abort with ^G)
1> c(gen_event_test).
gen_event_test.erl:2: Warning: undefined callback function code_change/3 (behaviour 'gen_event')
gen_event_test.erl:2: Warning: undefined callback function handle_call/2 (behaviour 'gen_event')
gen_event_test.erl:2: Warning: undefined callback function handle_info/2 (behaviour 'gen_event')
{ok,gen_event_test}
2> gen_event:start_link({local, event_manager}).
{ok,<0.39.0>}
3> gen_event:add_handler(event_manager, gen_event_test, 10).
10
ok
4> gen_event:add_handler(event_manager, gen_event_test, 20).
20
ok
5> gen_event:notify(event_manager, an_event).
an_event-20
an_event-10
ok
6> gen_event:notify(event_manager, an_event).
an_event-21
an_event-11
ok
7> gen_event:notify(event_manager, an_event).
an_event-22
an_event-12
ok
8> gen_event:stop(event_manager).
stop-23
stop-13
ok
9> 
