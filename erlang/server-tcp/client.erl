-module(client).
-export([client/1]).

times_do(0, _) ->
    ok;
times_do(N, F) ->
    F(N),
    times_do(N - 1, F).

client([NumeroProcesos]) ->
    SomeHostInNet = "localhost",
    times_do(list_to_integer(atom_to_list(NumeroProcesos)), fun(N) -> 
								    spawn(fun() -> client_connect(N, SomeHostInNet)
									  end)
							    end).

client_connect(N, SomeHostInNet) ->
    {ok, Sock} = gen_tcp:connect(SomeHostInNet, 5678, 
                                 [binary, {packet, 0}]),
    Mensaje = io_lib:format("Soy el proceso ~p\n", [N]),
    client_loop(Mensaje, Sock).

client_loop(Mensaje, Sock) ->
    ok = gen_tcp:send(Sock, Mensaje),
    client_loop(Mensaje, Sock).
    %% ok = gen_tcp:close(Sock).
