-module(server).
-export([server/1]).

times_do(0, _) ->
    ok;
times_do(N, F) ->
    F(),
    times_do(N - 1, F).

server([NumeroProcesos]) ->
    {ok, LSock} = gen_tcp:listen(5678, [binary, {packet, 0}, 
					{active, false}]),
    times_do(list_to_integer(atom_to_list(NumeroProcesos)), fun() -> 
								    spawn(fun() -> accept_loop(LSock) end)
							    end),
    timer:sleep(infinity).
    %% [spawn(fun() -> accept_loop(LSock) end) || _ <- lists:seq(1, 1000, 1)].

accept_loop(LSock) ->
    %% io:format("Process accepting...~p\n", [LSock]),
    {ok, Sock} = gen_tcp:accept(LSock),
    spawn(fun() -> do_recv(Sock) end),
    accept_loop(LSock).

do_recv(Sock) ->
    case gen_tcp:recv(Sock, 0) of
	{ok, <<"fin\r\n", _/binary>> } ->
	    io:format("Fin\n", []),
	    gen_tcp:close(Sock);
        {ok, B} ->
	    %% io:format("~p\n", [B]),
            do_recv(Sock);
	{error, closed} ->
	    io:format("Close\n", []),
	    gen_tcp:close(Sock)
    end.
