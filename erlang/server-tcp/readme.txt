Para poder arrancar el millón de procesos arrancar la EVM con:

	erl +P 2000000

Se puede comprobar con:

	erlang:system_info(process_limit).

Como muestra :)

	4> length(erlang:processes()).
	1000026
	Fin
	5> 


Arrancar el servidor desde línea de comando (10000 es el número de procesos a
arrancar pasado a la función principal server:server).

	 erl -compile server.erl
	 erl +P 2000000 -noshell -s server server 10000

Arrancar el cliente desde línea de comando.

	 erl -compile client.erl
	 erl +P 2000000 -noshell -s client client 10000
