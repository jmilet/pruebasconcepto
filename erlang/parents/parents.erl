-module(parents).
-include_lib("eunit/include/eunit.hrl").
-export([process/1, main/1]).

-define(SPACES, "    ").


process1("(" ++ T, Level, Acc) ->
    process1(T, Level + 1, Acc);
process1(")" ++ T, Level, Acc) ->
    process1(T, Level - 1, Acc);
process1([Val|T], Level, Acc) ->
    process1(T, Level, [string:copies(?SPACES, Level) ++ [Val] | Acc]);
process1([], _Level, Acc) ->
    Acc.

process(String) ->
    lists:reverse(process1(String, -1, [])).

main(_) ->
    Result = process("(a(b(c)b)a)"),
    
    lists:foreach(fun(X) ->
			  io:format("~p~n", [X])
		  end,
		  Result).

prueba0001_test() ->
    Input = "(a(b(c)b)a)",

    [R1, R2, R3, R4, R5] = process(Input),

    ?assert(R1 =:= string:copies(?SPACES, 0) ++ "a"),
    ?assert(R2 =:= string:copies(?SPACES, 1) ++ "b"),
    ?assert(R3 =:= string:copies(?SPACES, 2) ++ "c"),
    ?assert(R4 =:= string:copies(?SPACES, 1) ++ "b"),
    ?assert(R5 =:= string:copies(?SPACES, 0) ++ "a").    

