-module('compare').
-export([run/0]).


run() ->
    {ok, Fichero1} = file:open("a.txt", read),
    {ok, Fichero2} = file:open("b.txt", read),
    priv_run(file:read_line(Fichero1), file:read_line(Fichero2), Fichero1, Fichero2),
    file:close(Fichero2),
    file:close(Fichero1).

priv_run(eof, eof, _, _) ->
    ok;
priv_run(Linea1, eof, Fichero1, Fichero2) ->
    io:format("<<<~p~n", [Linea1]),
    priv_run(file:read_line(Fichero1), eof, Fichero1, Fichero2);
priv_run(eof, Linea2, Fichero1, Fichero2) ->
    io:format(">>>~p~n", [Linea2]),
    priv_run(eof, file:read_line(Fichero2), Fichero1, Fichero2);
priv_run(Linea1, Linea2, Fichero1, Fichero2) when Linea1 < Linea2 ->
    io:format("<<<~p~n", [Linea1]),
    priv_run(file:read_line(Fichero1), Linea2, Fichero1, Fichero2);
priv_run(Linea1, Linea2, Fichero1, Fichero2) when Linea1 > Linea2 ->
    io:format(">>>~p~n", [Linea2]),
    priv_run(Linea1, file:read_line(Fichero2), Fichero1, Fichero2);
priv_run(_Linea1, Linea2, Fichero1, Fichero2) ->
    io:format("===~p~n", [Linea2]),
    priv_run(file:read_line(Fichero1), file:read_line(Fichero1), Fichero1, Fichero2).

main(_) ->
    run().

