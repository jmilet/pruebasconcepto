-module(pingpong).
-export([test/1, pong/1]).

ping(_, 0) ->
    fin;
ping(Pid, N) ->
    io:format("ping~n"),
    Pid ! N - 1,
    receive
        _ -> ping(Pid, N - 1)
    end.

pong(Pid) ->
    receive
	N ->
	    io:format("pong~n"),
	    Pid ! pong,
	    case N of
		N when N > 0 ->
		    pong(Pid);
		_ ->
		    fin
	    end
    end.

test(N) ->
    Pong = spawn(pingpong, pong, [self()]),
    ping(Pong, N).
    
