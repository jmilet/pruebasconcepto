-module(test_mnesia).
-export([do_this_once/0, demo/0, demo2/0, start/0, stop/0]).


do_this_once()->
    mnesia:create_schema([node()]),
    mnesia:start(),
    mnesia:create_table(test, [{attributes, [id, funcion]}]),
    mnesia:stop().

start()->
    mnesia:start(),
    mnesia:wait_for_tables([test], 20000).

stop()->
    mnesia:stop().

demo()->
    start(),

    Y = 200,
    
    mnesia:transaction(fun()->
			       mnesia:write({test, 1, fun(X)->
							Y + X
						end})
		       end),

    {atomic, [{test, Id, Funcion}]} = mnesia:transaction(
	  fun()->
		  mnesia:read({test, 1})
	  end),
    io:format("~p~n", [Funcion(10)]).
    
    %%mnesia:stop().

demo2()->
    %%start(),

    {atomic, [{test, Id, Funcion}]} = mnesia:transaction(
	  fun()->
		  mnesia:read({test, 1})
	  end),
    io:format("~p~n", [Funcion(10)]),
    
    mnesia:stop().

