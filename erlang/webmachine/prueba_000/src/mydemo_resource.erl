%% @author author <author@example.com>
%% @copyright YYYY author.
%% @doc Example webmachine_resource.

-module(mydemo_resource).
-export([init/1, to_json/2, content_types_provided/2]).

-include_lib("webmachine/include/webmachine.hrl").

init([]) ->
    {ok, undefined}.

content_types_provided(ReqData, State) ->
    {[ {"application/json", to_json} ], ReqData, State}.

to_json(ReqData, State) ->
    dets:open_file(claves, {file, "/Users/jmimora/salida.txt"}),
    [{veces, N}] = dets:lookup(claves, veces),
    dets:insert(claves, {veces, N + 1}),
    dets:close(claves),

    Id = wrq:path_info(id, ReqData),
    Resp = mochijson:encode({struct, [{id, Id}, {veces, N}]}),
    {Resp, ReqData, State}.
