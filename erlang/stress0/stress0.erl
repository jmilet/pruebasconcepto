-module(stress0).
-export([start/0]).

start() ->
    spawn(fun() -> loop_principal(0) end).

loop_principal(20) ->
    ok;
loop_principal(N) ->
    spawn(fun() -> generador(N, string:copies("hola", 10000000), "") end),
    receive
	_ -> true
    after 0 ->
	    loop_principal(N + 1)
    end.

generador(N, Cadena, Total) ->
    if length(Total) rem 20000 =:= 0
       -> 
	    io:format("~p -> ~p\n", [N, length(Total)]);
       true -> true
    end,
    receive
	_ -> true
    after 0 ->
	    generador(N, Cadena, lists:append(Cadena, Total))
    end.
    
    
