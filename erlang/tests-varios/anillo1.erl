-module(anillo1).
-export([main/0, proceso1/0]).

proceso1() ->
    receive
	{Msg, From} ->
	    if Msg < 10000000 ->
		    %%io:format("~p -> ~p~n", [self(), Msg]),
		    From ! {Msg + 1, self()},
		    proceso1();
	       true ->
		    io:format("~p -> ~p~n", [self(), Msg])
	    end;
	start ->
	    P2 = spawn(anillo1, proceso1, []),
	    P2 ! {1, self()},
	    proceso1();
	_ ->
	    true
    end.

main()->
    P1 = spawn(anillo1, proceso1, []),
    P1 ! start.

