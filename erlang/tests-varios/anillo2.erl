-module(anillo2).
-export([main/0, proceso/1]).

proceso(Previous) ->
    receive
	token ->
	    io:format("~p~n", [self()]),
	    Previous ! token,
	    proceso(Previous);
	{set_previous, _Previous} ->
	    _Previous ! token,
	    proceso(_Previous)
    end.

run(1, Previous) ->
    Previous;
run(N, Previous) ->
    P = spawn(anillo2, proceso, [Previous]),
    run(N - 1, P).
    

main() ->
    P1 = spawn(anillo2, proceso, [false]),
    Last = run(10000, P1),
    P1 ! {set_previous, Last}.
