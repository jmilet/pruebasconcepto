-module(chaos).
-behaviour(gen_server).
-export([code_change/3, handle_call/3, handle_cast/2, handle_info/2, init/1, terminate/2, start_link/0, start_n_processes/1]).


start_n_processes(N) ->
    L1 = [chaos:start_link() || _ <- lists:seq(1, N)],
    L2 = [P || {ok, P} <- L1],
    lists:foreach(fun(P) ->
			  set_pid_list(P, L2)
		  end, L2),
    lists:foreach(fun(P) ->
			  run(P)
		  end, L2).

start_link() ->
    random:seed(now()),
    gen_server:start_link(?MODULE, [], []).

init(_) ->
    {ok, {[], 0}}.

handle_call({set_pid_list, List}, From, {_PidList, N}) ->
    {reply, ok, {List, N}};

handle_call(run, From, State) ->
    {reply, ok, State, 0}.

handle_cast({pregunta, From, NP}, {PidList, N}) ->
    Mayor = if NP >= N ->
		    NP;
	       true ->
		    N
	    end,
    io:format("pregunta : ~p -> ~p~n", [self(), Mayor]),
    gen_server:cast(From, {respuesta, Mayor + 1}),
    {noreply, {PidList, Mayor + 1}, tiempo()};

handle_cast({respuesta, NP}, {PidList, N}) ->
    Mayor = if NP >= N ->
		    NP;
	       true ->
		    N
	    end,
    io:format("respuest : ~p -> ~p~n", [self(), Mayor]),
    {noreply, {PidList, Mayor}, tiempo()}.

code_change(OldVsn, State, _Extra) ->
    {ok, State}.

handle_info(_Info, {PidList, N}) ->
    %io:format("Info ~p~n", [self()]),
    lists:foreach(fun(P) ->
			  gen_server:cast(P, {pregunta, self(), N + 1})
		  end, PidList),
    {noreply, {PidList, N + 1}, tiempo()}.

terminate(Reason, State) ->
    ok.

tiempo() ->
    X = (random:uniform(4) + 0) * 1000,
    %io:format("~p~n", [X]),
    X.

one_pid(PidList) ->
    N = length(PidList),
    %io:format(" ---------------> ~p~n", [randon:uniform(N)]),
    lists:nth(random:uniform(N), PidList).

set_pid_list(Pid, PidList) ->
    gen_server:call(Pid, {set_pid_list, lists:delete(Pid, PidList)}).

run(Pid) ->
    gen_server:call(Pid, run).

