-module(searchstring).
-export([start/0]).

start() ->
    io:format("~p\n", [search("hla",  "pues bien hola que tal")]),
    io:format("~p\n", [search("pues", "pues bien hola que tal")]),
    io:format("~p\n", [search("tal",  "pues bien hola que tal")]),
    io:format("~p\n", [search("hola", "pues bien hola que tal")]).


es_prefijo([C|Tail1], [C|Tail2]) ->
    es_prefijo(Tail1, Tail2);
es_prefijo([_C|_Tail1], [_D|_Tail2]) ->
    no;
es_prefijo([], _) ->
    yes;
es_prefijo(_, []) ->
    no.

search(S1, S2) ->
    search_loop(S1, S2, 0).

search_loop([C|S], [C|T], Posicion) ->
    %%io:format("iguales: ~p\n", [C]),
    case es_prefijo(S, T) of
	yes -> Posicion;
	no -> search_loop([C|S], T, Posicion)
    end;
search_loop([C|S], [_H|T], Posicion) ->
    %%io:format("diferentes: ~p\n", [H]),
    search_loop([C|S], T, Posicion + 1);
search_loop(_, [], _Posicion) ->
    not_found.

