-module(eunitest).
-include_lib("eunit/include/eunit.hrl").

start_stop_test_() ->
    {"Texto",
     {setup,
      fun start/0,
      fun stop/1,
      fun(State) -> [
      		    ?_test(prueba(State)),
      		    ?_test(prueba(State))]
      end}}.

start() ->
    ?debugFmt("Starting test...~n", []),
    {hola, que, tal}.

stop(State) ->
    ?debugFmt("Stoping test ~p~n", [State]).

prueba(State) ->
    ?debugFmt("my_test ~p~n", [State]),
    ?assertEqual(1, 1).
    
