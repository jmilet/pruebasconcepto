-module(time_procesos).
-export([start/0, proceso/0]).

do_times(0, _) ->
    ok;
do_times(N, F) ->
    F(),
    do_times(N - 1, F).

start() ->
    io:format("Inicio\n"),
    T1 = erlang:now(),
    do_times(250000, fun () ->
			     spawn(fun proceso/0)
		     end),
    T2 = erlang:now(),
    io:format("OK: ~p -> ~p microsegundos\n", [length(erlang:processes()), timer:now_diff(T2, T1)]).

proceso() ->
    receive
	_ ->
	    proceso
    end.

    
