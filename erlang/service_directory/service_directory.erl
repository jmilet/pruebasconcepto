-module(service_directory).
-behaviour(gen_server).
-export([start_link/0, init/1, add_resource/1, state/0, exchange_resources/0, stop/0, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

%--------------------------------------------------------------------------------------
% The process state.
% It will have two maps, one for the local resources and other for the foreign ones.
% Each key points to a list of nodes containing the resource.
%
%  {state,#{100 => ['dos@debian64-dev']},
%         #{1 => ['uno@debian64-dev'],2 => ['uno@debian64-dev']}}
%
%--------------------------------------------------------------------------------------
-record(state, {my_local=#{}, my_remote=#{}}).

% Define the contact node which will be used to know about other nodes.
-define(CONTACT_NODE, 'contact@debian64-dev').
-define(EXCHANGE_SECONDS, 10).

%--------------------------------------------------------------------------------------
% API
%--------------------------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

add_resource(ResourceName) ->
    gen_server:call(?MODULE, {add_resource, ResourceName}).

state() ->
    gen_server:call(?MODULE, state).

exchange_resources() ->
    gen_server:cast(?MODULE, exchange_resources).

stop() ->
    gen_server:cast(?MODULE, stop).

%--------------------------------------------------------------------------------------
% Callbacks
%--------------------------------------------------------------------------------------
init(_Args) ->
    % We force a timeout event returning 0 as timeout value.
    {ok, #state{}, 0}.

handle_call({add_resource, ResourceName}, _From, #state{my_local=MyLocal} = State) ->
    % Include the resource as local resource.
    MyLocal2 = maps:put(ResourceName, [node()], maps:remove(ResourceName, MyLocal)),
    gen_server:cast(?MODULE, exchange_resources),
    {reply, ok, State#state{my_local=MyLocal2}};
handle_call(state, _From, State) ->
    % Just return the process's state.
    {reply, State, State}.

handle_cast(exchange_resources, #state{my_local=MyLocal}) ->
    % For each known node, even this process, ask for an exchange of resources.
    lists:foreach(fun(Node) ->
			  gen_server:cast({?MODULE, Node}, {set_resources, MyLocal, node()}) end,
		  nodes()),
    {noreply, #state{my_local=MyLocal}};
handle_cast({set_resources, ItsLocal, From},
	    #state{my_local=MyLocal, my_remote=MyRemote} = State) ->
    % Other process is updating us about its resources.
    MyRemote2 = integrate_resources(MyRemote, ItsLocal),
    % We send to it ours.
    gen_server:cast({?MODULE, From}, {answer_resources, MyLocal, node()}),
    print_state(State#state{my_remote=MyRemote2}),
    {noreply, State#state{my_remote=MyRemote2}};
handle_cast({answer_resources, ItsLocal, _From},
	    #state{my_remote=MyRemote} = State) ->
    % Got the response of the other process. Just update our state with its info.
    MyRemote2 = integrate_resources(MyRemote, ItsLocal),
    {noreply, State#state{my_remote=MyRemote2}};
handle_cast(stop, State) ->
    {stop, normal, State}.
			
handle_info(timeout, State) ->
    % Take adavantge of the initial 0 timeout to contact the 'contact' node, so
    % each node introduce itself to the cluster.
    % Within a random interval upto ?EXCHANGE_SECONDS seconds we ask for another exchange.
    net_adm:ping(?CONTACT_NODE),
    exchange_resources(),
    timer:send_after(random:uniform(?EXCHANGE_SECONDS) * 1000, exchange_resources),
    {noreply, State};
handle_info(exchange_resources, State) ->
    exchange_resources(),
    print_state(State),
    timer:send_after(random:uniform(?EXCHANGE_SECONDS) * 1000, exchange_resources),
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    io:format("Code changed~n"),
    {ok, State}.

terminate(_Reason, _State) ->
    ok.

%--------------------------------------------------------------------------------------
% Private
%--------------------------------------------------------------------------------------
rec_integrate_resources([], _ItsResources, MyResources) ->
    MyResources;
rec_integrate_resources([H|T], ItsResources, MyResources) ->
    ItsNodes = maps:get(H, ItsResources),
    case maps:find(H, MyResources) of
	error -> MyNodes = [];
	{ok, MyNodes} -> MyNodes
    end,
    MyResources2 = maps:put(H, sets:to_list(sets:from_list(ItsNodes ++ MyNodes)), MyResources),
    rec_integrate_resources(T, ItsResources, MyResources2).

integrate_resources(MyResources, ItsResources) when ItsResources =:= #{} ->
    MyResources;
integrate_resources(MyResources, ItsResources) ->
    % Update our resources's structure with the received one.
    rec_integrate_resources(maps:keys(ItsResources), ItsResources, MyResources).

print_state(State) ->
    io:format("~p~n", [State]).
