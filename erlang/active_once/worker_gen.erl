-module(worker_gen).

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).

start_link(ASocket) ->
    io:format("worker_gen:start_link...~n"),
    gen_server:start_link(?MODULE, [ASocket], []).

init([ASocket]) ->
    io:format("worker_gen:init...~n"),
    {ok, ASocket}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Request, State) ->
    {noreply, State}.

handle_info({tcp, _S, Data}, ASocket) ->
    io:format("~p~n", [Data]),
    inet:setopts(ASocket, [{active, once}, {packet, line}]),
    timer:sleep(3000),
    {noreply, ASocket};

handle_info({tcp_closed, _S}, ASocket) ->
    io:format("Closed...~n"),
    {stop, normal, ASocket};

handle_info(timeout, ASocket) ->
    io:format("Closed...~n"),
    {stop, normal, ASocket}.

terminate(_Reason, ASocket) ->
    io:format("Terminated...~n"),
    gen_tcp:close(ASocket),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
