-module(listener_sup).

-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).


start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    io:format("listener_sup:init...~n"),

    {ok, LSocket} = gen_tcp:listen(2000, [{active, false}]),

    NumberOfWorkers = 3,
    RestartStrategy = one_for_one,
    MaxRestarts = 100,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = transient,
    Shutdown = 2000,
    Type = worker,

    ChildList = [{build_label("accept_fsm_", C),
		  {'accept_fsm', start_link, [LSocket]},
		  Restart, Shutdown, Type, ['accept_fsm']}
		 || C <- lists:seq(1, NumberOfWorkers)],

    {ok, {SupFlags, ChildList}}.

build_label(Name, C) ->
    io_lib:format("~s~w", [Name, C]).
