-module(accept_fsm).

-behaviour(gen_fsm).

-export([start_link/1]).
-export([init/1, handle_event/3,
	 handle_sync_event/4, handle_info/3, terminate/3, code_change/4]).
-export([accept/2]).

start_link(LSocket) ->
    io:format("accept_gen:start_link...~n"),
    gen_fsm:start_link(?MODULE, [LSocket], []).

init([LSocket]) ->
    io:format("accept_gen:init...~n"),
    gen_fsm:send_event(self(), accept),
    {ok, accept, LSocket}.

% Events.
accept(accept, LSocket) ->
    io:format("accept_gen:accept...~n"),
    {ok, ASocket} = gen_tcp:accept(LSocket),
    inet:setopts(ASocket, [{active, once}, {packet, line}]),
    worker_sup:start_child(ASocket),
    gen_fsm:send_event(self(), accept),
    {next_state, accept, LSocket}.

% All events.
handle_event(accept, _StateName, State) ->
    {next_state, accept, State}.

handle_sync_event(_Any, _From, _StateName, State) ->
    {reply, ok, accept, State}.

% OTP messeges.
handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.
