-module(main_sup).

-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).


start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    io:format("main_sup:init...~n"),

    RestartStrategy = one_for_one,
    MaxRestarts = 10,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = supervisor,

    ListenerSup = {'listener_sup',
		   {'listener_sup', start_link, []},
		   Restart, Shutdown, Type, ['listener_sup']},

    WorkerSup = {'worker_sup',
		   {'worker_sup', start_link, []},
		   Restart, Shutdown, Type, ['worker_sup']},

    ChildList = [ListenerSup, WorkerSup],

    {ok, {SupFlags, ChildList}}.
