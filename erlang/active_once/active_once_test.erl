-module(active_once_test).
-include_lib("eunit/include/eunit.hrl").

main_test() ->
    {ok, Supervisor} = listener_sup:start_link(),
    {ok, _Pid} = supervisor:start_child(Supervisor, []).
