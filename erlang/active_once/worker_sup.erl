-module(worker_sup).

-behaviour(supervisor).

-export([start_link/0, start_child/1]).
-export([init/1]).


start_link() ->
    io:format("listener_sup:start_link...~n"),
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_child(ASocket) ->
    {ok, Pid} = supervisor:start_child(?MODULE, [ASocket]),
    gen_tcp:controlling_process(ASocket, Pid),
    {ok, Pid}.

init([]) ->
    io:format("listener_sup:init...~n"),

    RestartStrategy = simple_one_for_one,
    MaxRestarts = 100,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = temporary,
    Shutdown = 2000,
    Type = worker,

    AChild = {'worker_gen', {'worker_gen', start_link, []},
	      Restart, Shutdown, Type, ['worker_gen']},

    {ok, {SupFlags, [AChild]}}.
