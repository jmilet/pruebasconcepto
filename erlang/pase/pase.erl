-module(pase).
-export([run/0, proceso/0, start/2]).

proceso() ->
	receive
		{suma, Remoto, X} ->
			io:format("~p~n", [X]),
			Remoto ! {suma, self(), X + 1},
			proceso();
		{start, NombreServidorRemoto} ->
			{proceso, NombreServidorRemoto} ! {suma, self(), 0},
			proceso()
	end.

start(PID, NombreServidorRemoto) ->
	PID ! {start, NombreServidorRemoto}.

run() ->
	Proceso = spawn(pase, proceso, []),
	register(proceso, Proceso),
	Proceso.