En un terminal:

user@BIOPACK-PC77 ~/erlang$ erl -sname gato@localhost -setcookie abc
Eshell V5.10.1  (abort with ^G)
(gato@localhost)1> c(pase).
{ok,pase}
(gato@localhost)2> pase:run().
<0.45.0>
(gato@localhost)3>


En un altre terminal:

user@BIOPACK-PC77 ~/erlang$ erl -sname perro@localhost -setcookie abc
Eshell V5.10.1  (abort with ^G)
(perro@localhost)1> c(pase).
{ok,pase}
(perro@localhost)2> Proceso = pase:run().
<0.45.0>
(perro@localhost)3> pase:start(Proceso, gato@localhost).

Té com a resultat en un terminal:

(perro@localhost)4> 4241
(perro@localhost)4> 4243
(perro@localhost)4> 4245
(perro@localhost)4> 4247
(perro@localhost)4> 4249
(perro@localhost)4> 4251
(perro@localhost)4> 4253
(perro@localhost)4> 4255
(perro@localhost)4> 4257
(perro@localhost)4> 4259
(perro@localhost)4> 4261

Té com a resultat en un altre terminal:

(gato@localhost)3> 4238
(gato@localhost)3> 4240
(gato@localhost)3> 4242
(gato@localhost)3> 4244
(gato@localhost)3> 4246
(gato@localhost)3> 4248
(gato@localhost)3> 4250
(gato@localhost)3> 4252
(gato@localhost)3> 4254
(gato@localhost)3> 4256
(gato@localhost)3> 4258
(gato@localhost)3> 4260
(gato@localhost)3> 4262
(gato@localhost)3> 4264
(gato@localhost)3> 4266
(gato@localhost)3> 4268

