
Si un proceso no refresca el módulo en dos compilaciones seguidas el sistema
lo termina porque lo considera incoherente.

c(modulo).   -> compila y carga el módulo.
l(modulo).   -> sólo carga el módulo.


