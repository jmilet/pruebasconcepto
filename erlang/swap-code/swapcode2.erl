-module(swapcode2).
-export([start/0, loop/0]).

start() ->
    spawn(fun loop/0).

loop() ->
    io:format("OK\n"),
    receive
    after 1000 ->
	    ?MODULE:loop()
    end.
