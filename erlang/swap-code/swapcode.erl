-module(swapcode).
-export([start/0, loop/0]).

start() ->
    spawn(fun loop/0).

loop() ->
    io:format("OKOKOK\n"),
    receive
	swap_code ->
	    ?MODULE:loop();
	_ ->
	    loop()
    after 1000 ->
	    loop()
    end.
