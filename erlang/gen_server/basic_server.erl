-module(basic_server).
-behaviour(gen_server).
-export([start_link/0, init/1, hola/1, adios/1, handle_call/3, handle_cast/2]).

start_link()->
        gen_server:start_link(?MODULE, [], []).

hola(Pid)->
        gen_server:call(Pid, hola).

adios(Pid)->
        gen_server:cast(Pid, adios).

init([])->
        io:format("En init~n"),
        {ok, []}.

handle_call(hola, _From, State)->
        io:format("hola~n"),
        {reply, ok, State}.

handle_cast(adios, State)->
        io:format("Comienzo del adiós~n"),
        timer:sleep(2000),
        io:format("Final del adiós~n"),
        {noreply, State}.
