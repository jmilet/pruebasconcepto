-module(basic_supervisor).
-behaviour(supervisor).
-export([start_link/0, init/1]).

start_link()->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([])->
    {ok,{
       {one_for_one, 3, 60},
       [{servidor_basico1,
    	 {basic_server, start_link, []},
    	 permanent, 10, worker,
    	 [basic_server]},
    	{servidor_basico2,
    	 {basic_server, start_link, []},
    	 permanent, 10, worker,
    	 [basic_server]}
       ]}}.
