-module(server).
-export([start/0]).


start() ->
    {ok, LSock} = gen_tcp:listen(5678, [binary, {packet, 0}, 
					{active, false}]),
    spawn(fun() -> accept_loop(LSock) end),
    timer:sleep(infinity).

accept_loop(LSock) ->
    %% io:format("Process accepting...~p\n", [LSock]),
    {ok, Sock} = gen_tcp:accept(LSock),
    NuevoProceso = spawn(fun() -> do_recv(Sock, []) end),
    gen_tcp:send(Sock, io_lib:format("iniciado ~p\n", [NuevoProceso])),
    accept_loop(LSock).

do_recv(Sock, Procesos) ->
    case gen_tcp:recv(Sock, 0) of
	{ok, <<"fin\r\n", _/binary>> } ->
	    io:format("Fin\n", []),
	    gen_tcp:close(Sock);

	{ok, <<"crear\r\n", _/binary>> } ->
	    io:format("crear\n"),
	    NuevoProceso = crear_proceso(Sock),
	    gen_tcp:send(Sock, io_lib:format("Proceso creado ~p\n", [NuevoProceso])),
	    Procesos2 = [NuevoProceso|Procesos],
	    listar_procesos(Procesos2),
	    do_recv(Sock, Procesos2);

	{ok, <<"crear_10\r\n", _/binary>> } ->
	    Procesos2 = lists:foldl(fun (_, ProcesosClosure) ->
					    io:format("crear\n"),
					    NuevoProceso = crear_proceso(Sock),
					    gen_tcp:send(Sock, io_lib:format("Proceso creado ~p\n", [NuevoProceso])),
					    [NuevoProceso|ProcesosClosure]
				    end, [], lists:seq(1, 10, 1)),
	    Procesos3 = lists:append(Procesos2, Procesos),
	    listar_procesos(Procesos3),
	    do_recv(Sock, Procesos3);


	{ok, <<"finalizar\r\n", _/binary>> } when length(Procesos) > 0 ->
	    [Proceso|Procesos2] = Procesos,
	    listar_procesos(Procesos2),
	    Proceso ! fin,
	    gen_tcp:send(Sock, io_lib:format("finalizado ~p\n", [self()])),
	    if length(Procesos2) > 0 ->
		    do_recv(Sock, Procesos2);
	       true -> true
	    end;

	{ok, <<"finalizar\r\n", _/binary>> } when Procesos == [] ->
	    gen_tcp:send(Sock, "No hay procesos que finalizar\n");

	{ok, <<"listar\r\n", _/binary>> } ->
	    listar_procesos(Procesos),
	    do_recv(Sock, Procesos);

        {ok, Bytes} ->
	    %% io:format("~p\n", [B]),
	    gen_tcp:send(Sock, Bytes),
            do_recv(Sock, Procesos);

	{error, closed} ->
	    [Proceso ! fin || Proceso <- Procesos],
	    io:format("~p -> Close\n", [self()])
    end.

crear_proceso(Sock) ->
    spawn_link(
      fun () ->
	      proceso_loop(0, Sock, self())
      end).
			     
proceso_loop(N, Sock, Pid) ->
    gen_tcp:send(Sock, io_lib:format("~p -> ~p\n", [Pid, N])),
    receive
	fin -> fin
    after
	3000 ->
	    proceso_loop(N + 1, Sock, Pid)
    end.

listar_procesos(Procesos) ->
    io:format("~p -> ~p\n", [self(), Procesos]).

