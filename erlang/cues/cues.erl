-module(cues).
-export([init/0, main/0, worker/1]).



init() ->
    spawn(fun main/0).

main() ->
    Procesos = [spawn(?MODULE, worker, [self()]) || _X <- lists:seq(0, 10)],
    loop(Procesos, []).

loop(Procesos, Pendientes) ->
    receive
	{get, From} ->
	    case Procesos of
		[] ->
		    loop(Procesos, [From|Pendientes]);
		_Else ->
		    From ! hd(Procesos),
		    loop(tl(Procesos), Pendientes)
	    end;
	{put, P} ->
	    case Pendientes of
		[Pendiente|Resto] ->
		    Pendiente ! P,
		    loop(Procesos, Resto);
		_Else ->
		    loop([P|Procesos], Pendientes)
	    end;
	show ->
	    io:format("~p", [Procesos]),
	    loop(Procesos, Pendientes)
    end.
			    
worker(Parent) ->
    receive
	_X ->
	    io:format("ups..."),
	    Parent ! {put, self()},
	    worker(Parent)
    end.
	    
