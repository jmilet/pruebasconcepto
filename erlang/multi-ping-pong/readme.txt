
Ejecuta 1000000 de veces un ping-pong entre dos procesos.
Al final han de quedar 2000001 procesos corriendo (al
main se le hace un timer:sleep(infinity), pero durante
la creación se hace spawn de un intermedio de arranque.

erl +P 3000000 -noshell -s multi multi 1000000 -sname nodo1

Se le pasa nombre de nodo porque después nos interesa
conectarnos a él así:

$ erl -sname nodo2
Erlang R15B (erts-5.9) [source] [smp:2:2] [async-threads:0] [hipe] [kernel-poll:false]

Eshell V5.9  (abort with ^G)
(nodo2)1>  
User switch command
 --> r 'nodo1@air-2' shell
 --> j
   1  {shell,start,[init]}
   2* {'nodo1',shell,start,[]}
 --> c 2
Eshell V5.9  (abort with ^G)
(nodo1)1> 

