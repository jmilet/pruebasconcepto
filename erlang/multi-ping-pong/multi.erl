-module(multi).
-export([multi/1]).


do_times(0, _) ->
    ok;
do_times(N, F) ->
    F(),
    do_times(N - 1, F).

multi([NumeroProcesos]) ->
    do_times(list_to_integer(atom_to_list(NumeroProcesos)), fun() ->
								    spawn(fun () -> spawn_pair() end)
							    end),
    timer:sleep(infinity).

spawn_pair() ->
    Uno = spawn(fun() ->
			 proceso(0)
		end),
    Dos = spawn(fun() ->
			 proceso(0)
		end),
    Uno ! {set_other_pid, Dos},
    Dos ! {set_other_pid, Uno},
    Uno ! {start}.

proceso(OtherPid) ->
    receive
	{set_other_pid, ParamOtherPid} ->
	    proceso(ParamOtherPid);
	{start} ->
	    OtherPid ! {mensaje, "hola"},
	    proceso(OtherPid);
	{mensaje, Mensaje} ->
	    %% io:format("~p, ~p\n", [self(), Mensaje]),
	    OtherPid ! {mensaje, Mensaje},
	    proceso(OtherPid)
    end.
	    
