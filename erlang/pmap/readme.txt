



15> timer:tc(pmap, pmap, [fun(X) -> X + 1 end, lists:seq(1, 1000000)]).
{5492339,
 [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
  24,25,26,27,28|...]}
16> timer:tc(pmap, pmap, [fun(X) -> X + 1 end, lists:seq(1, 1000000)]).
{5243988,
 [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
  24,25,26,27,28|...]}
17> Timer:tc(pmap, pmap, [fun(X) -> X + 1 end, lists:seq(1, 10000000)]).
{58799326,
 [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
  24,25,26,27,28|...]}



18> pmap:pmap(fun(X) -> X + 1 end, lists:seq(1, 1000000)).
[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
 24,25,26,27,28,29,30|...]

spawn(fun() -> pmap:pmap(fun(X) -> X + 1 end, lists:seq(1, 1000000)) end).



24> spawn(fun() -> pmap:pmap(fun(X) -> X + 1 end, lists:seq(1, 10000000)) end).
<0.31270.857>
25> length(erlang:processes()).
102
26> length(erlang:processes()).
145




spawn(fun() -> pmap:pmap(fun(X) -> lists:seq(1, 2000000) end, lists:seq(1, 10000000)) end).

