-module(pmap2).
-export([start/0]).

start()->
    io:format("~p\n", [timer:tc(fun run/1, [1000])]).

run(NumElementos)->
    L = lists:seq(1, NumElementos, 1),
    Resultado = pmap(fun(X) ->
			     X * 10
		     end, L),
    Suma = lists:foldr(fun(X, Sum) -> X + Sum end, 0, Resultado),
    io:format("~p\n", [Suma]).

pmap(Funcion, L)->
    Parent = self(),
    Pids = lists:map(fun(Elem)->
			     spawn(fun()->
					   Parent ! {self(), Funcion(Elem)}
				   end)
		     end, L),
    lists:map(fun(Pid)->
		      receive {Pid, Result} ->
			      Result
		      end
	      end, Pids).
