#!/usr/local/bin/escript


includes([Element|_Tail], Element) ->
    true;
includes([_Other|Tail], Element) ->
    includes(Tail, Element);
includes([], _Element) ->
    false.

p_recorrer([Parametro|Tail], Res, Tokens, LastToken) ->
    case includes(Tokens, Parametro) of
	true ->
	    p_recorrer(Tail, Res, Tokens, Parametro);
	false ->
	    p_recorrer(Tail, [{LastToken, Parametro} | Res], Tokens, LastToken)
    end;
p_recorrer([], Res, _Tokens, _LastToken) ->
    Res.

recorrer(Tokens, Args) ->
    lists:reverse(p_recorrer(Args, [], Tokens, none)).



main(Args) ->
    Res = recorrer(["-to", "-from"], Args),
    io:format("~p~n", [[Valor || {Param, Valor} <- Res, Param =:= "-to"]]),
    io:format("~p~n", [[Valor || {Param, Valor} <- Res, Param =:= "-from"]]).
