-module(fifo).
-export([new/1, insert/2, test/0]).

new(Size) ->
    {Size, 0, []}.

insert({Size, N, List}, Elem) ->
    if N < Size ->
	    {Size, N + 1, [Elem | List]};
       true ->
	    {Size, Size, [Elem | lists:reverse(tl(lists:reverse(List)))]}
    end.

test() ->
    A1 = new(3),
    
    A2 = insert(A1, 1),
    {3, 1, [1]} = A2,

    A3 = insert(A2, 2),
    {3, 2, [2, 1]} = A3,

    A4 = insert(A3, 3),
    {3, 3, [3, 2, 1]} = A4,

    A5 = insert(A4, 4),
    {3, 3, [4, 3, 2]} = A5,

    A6 = insert(A5, 5),
    {3, 3, [5, 4, 3]} = A6.


