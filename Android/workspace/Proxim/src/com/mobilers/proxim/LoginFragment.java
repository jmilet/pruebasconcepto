package com.mobilers.proxim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginFragment extends Fragment {
	
	private Button mConectarButton;
	private EditText mCodigoUsuarioEditText;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
		View v = inflater.inflate(R.layout.fragment_login, parent, false);
		
		mConectarButton = (Button) v.findViewById(R.id.boton_conectar);
		mCodigoUsuarioEditText = (EditText) v.findViewById(R.id.texto_usuario);
		mConectarButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				String codigoUsuario = mCodigoUsuarioEditText.getText().toString(); 
						
				if (codigoUsuario.length() > 0){
					//Toast.makeText(getActivity(), codigoUsuario, Toast.LENGTH_SHORT).show();
					
					Intent i = new Intent(getActivity(), UserListActivity.class);
					startActivity(i);
					
				}
				
			}
		});
		
		return v;
	}
}
